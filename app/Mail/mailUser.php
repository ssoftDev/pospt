<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class mailUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($detail)
    {
        //
        $this->detail = $detail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->detail['store'])&&isset($this->detail['bu'])){
            return $this
            ->from('pptserversmtp@gmail.com', $this->detail['title'])
            ->subject($this->detail['title'])
            ->view('mail.mailuser')
            ->with(['detail'=> $this->detail]);
        }else if(isset($this->detail['store'])&&!isset($this->detail['bu'])){
            return $this
            ->from('pptserversmtp@gmail.com', $this->detail['title'])
            ->subject($this->detail['title'])
            ->view('mail.mailAdmin')
            ->with(['detail'=> $this->detail]);
        }else{
              return $this
            ->from('pptserversmtp@gmail.com', $this->detail['title'])
            ->subject($this->detail['title'])
            ->view('mail.mailSuperAdmin')
            ->with(['detail'=> $this->detail]);
        }
    
       
    }
}
