<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MP_Product extends Model
{
    //
    protected $table = 'mp_product';
    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'pid', 'pid');
    }

    public function GroupProduct(){
        return $this->belongsTo('App\Models\GroupProduct', 'gp_id', 'gp_id');
    }
}
