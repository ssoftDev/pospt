<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $table = 'product';

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function Typeproduct()
    {
        return $this->belongsTo('App\Models\TypeProduct', 'type_product_id', 'type_product_id');
    }

    public function GroupProduct()
    {
        return $this->belongsTo('App\Models\MP_Product', 'pid', 'pid');
    }
}
