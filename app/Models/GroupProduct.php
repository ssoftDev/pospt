<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupProduct extends Model
{
    //
    protected $table = 'group_product';

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public $timestamps = false;

    public function GroupProductTemplate()
    {
        return $this->belongsTo('App\Models\GroupTemplateProduct', 'gt_id', 'gt_id');
    }

    public function BusinessUnit()
    {
        return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
    }

    

}
