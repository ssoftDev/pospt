<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranSectionRedeem extends Model
{
    //

    protected $table = 'transections_redeem';

    public function Redeem()
    {
        return $this->belongsTo('App\Models\Redeem', 'rd_id', 'rd_id');
    }

    public function Store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id', 'store_id');
    }
}
