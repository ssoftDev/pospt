<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MP_Product_Template extends Model
{
    //
    protected $table = 'mp_template_product';
    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    protected $primaryKey = null;

    public function GroupProduct()
    {
        return $this->belongsTo('App\Models\GroupProduct', 'gp_id', 'gp_id');
    }
}
