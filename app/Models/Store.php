<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
    protected $table = 'store';
    //  protected $fillable = ['user','pass','store_id','create_by','update_by'];

    protected $primaryKey = 'store_id';

    public $incrementing = true;

    public $timestamps = false;

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function BusinessUnit()
    {
        return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
    }

    public function ProductTemplate()
    {
        return $this->belongsTo('App\Models\ProductTemplate', 'tmppd_id', 'tmppd_id');
    }

    public function TemplateRedeem()
    {
        return $this->belongsTo('App\Models\TemplateRedeem', 'tmprd', 'tmprd_id');
    }
}
