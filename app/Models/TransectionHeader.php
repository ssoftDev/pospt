<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransectionHeader extends Model
{
    //
    protected $table = 'transection_header';


    public function Store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id', 'store_id');
    }
}
