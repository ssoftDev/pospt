<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MP_Earn extends Model
{
    //

    protected $table = 'mp_earn_product';
    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';
}
