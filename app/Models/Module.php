<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //

    protected $table = 'module';


    public static function makeInfo($id)
	{
		$row =  \DB::table('module')->where('module_name', $id)->get();
		
		$data = array();
		//dd($row);
		foreach($row as $r)
		{
			$data['id']		= $r->module_id;
			$data['title'] 	= $r->module_title;
		}

		return $data;


    }

    public static function validAccess($id)
	{

  

		$row = \DB::table('group_permission')->where('module_id', $id)
				->where('group_user_id', trim(\Auth::user()->group_user_id))
				->get();

				// dd(\Auth::user());
    
                
       
//		exit();

		if(count($row) >= 1)
		{
			$row = $row[0];
			if($row->access_data !='')
			{
				$data = json_decode($row->access_data,true);
			} else {
				$data = array();
			}
			return $data;

		} else {
			return false;
		}

	}
}
