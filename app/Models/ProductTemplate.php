<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTemplate extends Model
{
    //
        //
        protected $table = 'template_product';

        const CREATED_AT = 'create_date';
        const UPDATED_AT = 'update_date';
    
        public function BusinessUnit()
        {
            return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
        }
    
}
