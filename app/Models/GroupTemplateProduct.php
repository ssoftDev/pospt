<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupTemplateProduct extends Model
{
    //
    protected $table = 'proup_product_tab';

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public $timestamps = false;

    public function BusinessUnit()
    {
        return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
    }

    
}
