<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Redeem extends Model
{
    //

    protected $table = 'redeem_master';

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public $timestamps = false;

    public function BusinessUnit()
    {
        return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
    }

}
