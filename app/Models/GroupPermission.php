<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupPermission extends Model
{
    //

    protected $table = 'group_permission';

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public $timestamps = false;


    public function GroupUser()
    {
        return $this->belongsTo('App\Models\GroupUser', 'group_user_id', 'group_user_id');
    }

    public function Module()
    {
        return $this->belongsTo('App\Models\Module', 'module_id', 'module_id');
    }
}
