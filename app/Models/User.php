<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    //
    protected $table = 'authen';
    protected $fillable = ['user', 'pass', 'store_id', 'create_by', 'update_by'];

    protected $hidden = [
        'pass', 'remember_token',
    ];

    public $timestamps = false;

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    protected $primaryKey = 'user';
    
    public $incrementing = false;

    public function Store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id', 'store_id');
    }

    public function getAuthPassword()
    {
        return $this->pass;
    }

    public function BusinessUnit()
    {
        return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
    }


}
