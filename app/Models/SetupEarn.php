<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetupEarn extends Model
{
    //

    protected $table = 'setup_earn';

     // protected $dateFormat = 'd/m/y H:i:s';


    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';
    

    public function BusinessUnit()
    {
        return $this->belongsTo('App\Models\BusinessUnit', 'bu_id', 'bu_id');
    }

    public function MP(){
        return $this->belongsTo('App\Models\MP_Earn', 'eid', 'eid');
    }
}
