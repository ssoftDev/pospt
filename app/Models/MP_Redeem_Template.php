<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MP_Redeem_Template extends Model
{
    //

    protected $table = 'mp_template_redeem';
    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    protected $primaryKey = null;

    public function Redeem()
    {
        return $this->belongsTo('App\Models\Redeem', 'rd_id', 'rd_id');
    }
}
