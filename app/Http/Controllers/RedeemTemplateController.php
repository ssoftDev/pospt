<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\Module;
use App\Models\MP_Redeem_Template;
use App\Models\Redeem;
use App\Models\RedeemTemplate;
use App\Models\Store;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class RedeemTemplateController extends Controller
{
    //

    public $module = 'manage/redeem-template';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $RedeemTemplate = new RedeemTemplate;

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) != "1") {

            if (!empty($input['q'])) {
                $RedeemTemplate = $RedeemTemplate->where('bu_id', trim(\Auth::user()->bu_id))->where('tmprd_name', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
            } else {
                $RedeemTemplate = $RedeemTemplate->where('bu_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
            }

        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                 if (!empty($input['q'])) {
                    $RedeemTemplate = $RedeemTemplate->where('bu_id',$request->session()->get('bu'))->where('tmprd_name', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $RedeemTemplate = $RedeemTemplate->where('bu_id',$request->session()->get('bu'))->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                }
            } else {

                if (!empty($input['q'])) {
                    $RedeemTemplate = $RedeemTemplate->where('tmprd_name', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $RedeemTemplate = $RedeemTemplate->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                }
            }

        }

        return view('manage.redeemTemplate.redeemTemplate', compact('RedeemTemplate'));
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $RedeemTemplate = new RedeemTemplate;

        $MP_Redeem_Template = new MP_Redeem_Template;

        $edit = $RedeemTemplate->where('tmprd_id', $id)->get();

        $MP_Redeem_Template = $MP_Redeem_Template->where('tmprd_id', $id)->orderBy('seq', 'ASC')->get();

        $businessUnit = new BusinessUnit;
        $businessUnit = $businessUnit->get();

        $Redeem = new Redeem;
        $Redeem = $Redeem->get();

        return view('manage.redeemTemplate.formredeemTemplate', compact(['edit', 'MP_Redeem_Template', 'businessUnit', 'Redeem']));
    }

    public function create()
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;

        if (trim(\Auth::user()->group_user_id) != "1") {

            // $store = new Store;
            // $store = $store->where('store_id',\Auth::user()->store_id)->with('BusinessUnit')->first();

            $businessUnit = $businessUnit->where('bu_id', trim(\Auth::user()->bu_id))->get();

            // dd($store[0]->bu_id);
        } else {
            $businessUnit = $businessUnit->get();
        }

        return view('manage.redeemTemplate.formredeemTemplate', compact('businessUnit'));
    }

    public function store(Request $request)
    {

        //dd($request->all());

        $RedeemTemplate = new RedeemTemplate;

        $RedeemTemplate->tmprd_id = ($RedeemTemplate->max('tmprd_id') + 1) . '';

        $RedeemTemplate->tmprd_name = $request->input('t_name');

        $RedeemTemplate->bu_id = $request->input('businessunit');

        $RedeemTemplate->create_date = Carbon::now();

        $RedeemTemplate->create_by = \Auth::user()->user;

        $RedeemSeq = json_decode($request->input('tableredeem'));

        if ($RedeemSeq != null) {

            foreach ($RedeemSeq as $key => $value) {

                DB::table('mp_template_redeem')->insert([
                    ['tmprd_id' => $RedeemTemplate->tmprd_id,
                        'rd_id' => $value->id,
                        'seq' => $key + 1,
                    ],
                ]);

            }

        }

        $RedeemTemplate->save();

        flashMe()->success();
        return redirect('manage/redeem-template');
    }

    public function update(Request $request)
    {

        $RedeemTemplate = new RedeemTemplate;

        $updateRedeemTemplate['tmprd_name'] = $request->input('t_name');

        $updateRedeemTemplate['update_date'] = Carbon::now();

        $updateRedeemTemplate['update_by'] = \Auth::user()->user;

        // $updateRedeemTemplate['bu_id'] = $request->input('businessunit');

        $RedeemSeq = json_decode($request->input('tableredeem'));

        if ($RedeemSeq != null) {

            $MP_Redeem_Template = new MP_Redeem_Template;

            $MP_Redeem_Template->where('tmprd_id', $request->input('id'))->delete();

            foreach ($RedeemSeq as $key => $value) {

                DB::table('mp_template_redeem')->insert([
                    ['tmprd_id' => $request->input('id'),
                        'rd_id' => $value->id,
                        'seq' => $key + 1,
                    ],
                ]);

            }

        }

        $RedeemTemplate->where('tmprd_id', $request->input('id'))
            ->update($updateRedeemTemplate);

        flashMe()->success();

        return redirect('manage/redeem-template');
    }

    public function getRedeem(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');

            $dataTable = $request->get('data');

            $idData = [];

            if ($dataTable != null) {
                foreach ($dataTable as $key => $value) {
                    array_push($idData, $value['id']);
                }
            }

            $Redeem = new Redeem;
            $data = $Redeem
                ->where('bu_id', $query)
                ->WhereNotIn('rd_id', $idData)
                ->get();
            $output = '';
            foreach ($data as $row) {
                $output .= '<option value="' . $row->rd_id . '"' . 'name="' . $row->rd_code . '"' . '>' . $row->rd_desc . '</option>';
            }
            echo $output;
        }

        //  return response()->json([
        //     'dataTable' =>  $request->all()
        // ]);

    }

    public function destroy(Request $request)
    {
        //  dd($id);

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $RedeemTemplate = new RedeemTemplate();
        $RedeemTemplate->where('tmprd_id', $request->input('iddelete'))->delete();

        $MP_Redeem_Template = new MP_Redeem_Template;

        $MP_Redeem_Template->where('tmprd_id', $request->input('iddelete'))->delete();

        flashMe()->success();
        return redirect('manage/redeem-template');
    }

}
