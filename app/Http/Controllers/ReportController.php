<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\Store;
use App\Models\TransectionDetail;
use App\Models\TransectionHeader;
use App\Models\TranSectionRedeem;
use Carbon\Carbon;
use CSVReport;
use DateTime;
use DB;
use ExcelReport;
use Illuminate\Http\Request;
use PdfReport;

class ReportController extends Controller
{
    //

    public function index(Request $request)
    {

        $businessUnit = new BusinessUnit;

        $businessUnit = $businessUnit->get();

        $input['store'] = "";

        $input['report'] = "";

        $input['report_type'] = "";

        $input['start_end'] = "";

        $input['maxcard_no'] = "";

        $Store = new Store;

        if (trim(\Auth::user()->group_user_id) != "1") {
            if (trim(\Auth::user()->group_user_id) == "2") {
                $Store = $Store->where('bu_id', \Auth::user()->bu_id)->get();
            } else {

                $Store = $Store->where('bu_id', \Auth::user()->bu_id)->where('store_id', \Auth::user()->store_id)->get();
            }

        } else {
            $Store = $Store->get();
        }

        $TranSectionRedeem = null;
        $SummaryRedeem = null;
        $TransectionHeader = null;
        $TransectionDetail = null;
        $SummaryEarn = null;

        return view('manage.report.formReport', compact(['Store', 'TranSectionRedeem', 'SummaryRedeem', 'input', 'TransectionHeader', 'TransectionDetail', 'SummaryEarn']));
    }

    public function Report(Request $request)
    {

        $splitTime = explode(" - ", $request->input('start_end')); // dd($request->all());

        $start_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[0]));
        $end_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[1]));

        $Store = new Store;

        $Store = $Store->get();

        $storeid = [];
        if (trim(\Auth::user()->group_user_id) == "2") {
            $storeAdmin = Store::where('bu_id', \Auth::user()->bu_id)->get();

            foreach ($storeAdmin as $key => $value) {
                array_push($storeid, $value->store_id);
            }
        }

        // dd($storeid);

        $TranSectionRedeem = null;
        $SummaryRedeem = null;
        $TransectionHeader = null;
        $TransectionDetail = null;
        $SummaryEarn = null;

/////////////////////////////////////// transection redeem

        if ($request->input('report') == 'tredeem') {
            $TranSectionRedeem = new TranSectionRedeem;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($request->input('store') == 'all') {
                        $TranSectionRedeem = $TranSectionRedeem->with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                    } else {
                        $TranSectionRedeem = $TranSectionRedeem->with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $request->input('store'))->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                    }
                } else {

                    if ($request->input('store') == 'all') {
                        $TranSectionRedeem = $TranSectionRedeem->with(['Redeem', 'Store'])->where('store_id', \Auth::user()->store_id)->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->get();
                    } else {
                        $TranSectionRedeem = $TranSectionRedeem->with(['Redeem', 'Store'])->where('store_id', \Auth::user()->store_id)->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $request->input('store'))->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                    }
                }

            } else {
                if ($request->input('store') == 'all') {
                    $TranSectionRedeem = $TranSectionRedeem->with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                } else {
                    $TranSectionRedeem = $TranSectionRedeem->with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $request->input('store'))->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->where('void_status', '=', "n")->get();
                }
            }

        }

        /////////////////////////////////////// summary redeem

        if ($request->input('report') == 'sredeem') {

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($request->input('store') == 'all') {
                        $SummaryRedeem = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->whereIn('transections_redeem.store_id', $storeid)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();

                        // >with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->get();
                    } else {
                        $SummaryRedeem = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        // ->whereIn('transections_redeem.store_id', $storeid)
                            ->where('transections_redeem.store_id', $request->input('store'))
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();

                    }
                } else {

                    if ($request->input('store') == 'all') {

                        $SummaryRedeem = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        // ->whereIn('transections_redeem.store_id', $storeid)
                            ->where('transections_redeem.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();

                        // >with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->get();
                    } else {
                        $SummaryRedeem = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        // ->whereIn('transections_redeem.store_id', $storeid)
                            ->where('transections_redeem.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();
                    }
                }

            } else {
                if ($request->input('store') == 'all') {
                    $SummaryRedeem = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                        ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                        ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transections_redeem.store_id', '!=', null)
                        ->where('transections_redeem.rd_id', '!=', null)
                        ->where('transections_redeem.void_status', '=', "n")
                        ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                    // ->whereIn('transections_redeem.store_id', $storeid)
                    // ->where('transections_redeem.store_id', \Auth::user())
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->get();
                    // >with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->get();
                } else {
                    $SummaryRedeem = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                        ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                        ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transections_redeem.store_id', '!=', null)
                        ->where('transections_redeem.rd_id', '!=', null)
                        ->where('transections_redeem.void_status', '=', "n")
                        ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                    // ->whereIn('transections_redeem.store_id', $storeid)
                        ->where('transections_redeem.store_id', $request->input('store'))
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->get();
                }
            }

        }

        /////////////////////////////////////// Transaction Earn Header

        if ($request->input('report') == 'tearn') {
            $TransectionHeader = new TransectionHeader;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($request->input('store') == 'all') {
                        $TransectionHeader = $TransectionHeader->with(['Store'])->where('store_id', '!=', null)->where('maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                    } else {
                        $TransectionHeader = $TransectionHeader->with(['Store'])->where('store_id', '!=', null)->where('maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')->where('store_id', $request->input('store'))->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                    }
                } else {

                    if ($request->input('store') == 'all') {
                        $TransectionHeader = $TransectionHeader->with(['Store'])->where('store_id', \Auth::user()->store_id)->where('maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')->where('store_id', '!=', null)->where('void_status', '=', "n")->get();
                    } else {
                        $TransectionHeader = $TransectionHeader->with(['Store'])->where('store_id', \Auth::user()->store_id)->where('maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')->where('store_id', '!=', null)->where('store_id', $request->input('store'))->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                    }
                }

            } else {
                if ($request->input('store') == 'all') {
                    $TransectionHeader = $TransectionHeader->with(['Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->where('maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->get();
                } else {
                    $TransectionHeader = $TransectionHeader->with(['Store'])->where('store_id', '!=', null)->where('maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')->where('store_id', $request->input('store'))->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->where('void_status', '=', "n")->get();
                }
            }

        }

        /////////////////////////////////////// Transaction Earn Detail

        if ($request->input('report') == 'dearn') {

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($request->input('store') == 'all') {
                        $TransectionDetail = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->where('transection_header.maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->whereIn('transection_header.store_id', $storeid)
                            ->get();

                        // >with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->get();
                    } else {
                        $TransectionDetail = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', $request->input('store'))
                            ->get();

                    }
                } else {

                    if ($request->input('store') == 'all') {

                        $TransectionDetail = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->get();

                    } else {
                        $TransectionDetail = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->get();
                    }
                }

            } else {
                if ($request->input('store') == 'all') {
                    $TransectionDetail = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                        ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                        ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                        ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->get();
                } else {
                    $TransectionDetail = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                        ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                        ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                        ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.maxcard_id', 'like', '%' . $request->input('maxcard_no') . '%')
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->where('transection_header.store_id', $request->input('store'))
                        ->get();
                }
            }

        }

        /////////////////////////////////////// summary Earn

        if ($request->input('report') == 'searn') {

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($request->input('store') == 'all') {
                        $SummaryEarn = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->whereIn('transection_header.store_id', $storeid)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();

                        // >with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->get();
                    } else {

                        $SummaryEarn = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', $request->input('store'))
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();

                    }
                } else {

                    if ($request->input('store') == 'all') {

                        $SummaryEarn = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();

                    } else {
                        $SummaryEarn = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->get();
                    }
                }

            } else {
                if ($request->input('store') == 'all') {

                    $SummaryEarn = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->get();

                } else {

                    $SummaryEarn = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->where('transection_header.store_id', $request->input('store'))
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->get();

                }
            }

        }

        $input = $request->all();

        // dd($input['report']);

        return view('manage.report.formReport', compact(['Store', 'TranSectionRedeem', 'SummaryRedeem', 'input', 'start_time', 'end_time', 'TransectionHeader', 'TransectionDetail', 'SummaryEarn']));
    }

    public function displayReport(Request $request, $type, $report, $store, $start_end, $maxcard = "")
    {

        // dd($maxcard);

        $start_end = str_replace('^', '/', str_replace('&&', ' - ', $start_end));

        $splitTime = explode(" - ", $start_end); // dd($request->all());

        $start_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[0]));
        $end_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[1]));

        $Store = new Store;

        $Store = $Store->get();

        $storeid = [];
        if (trim(\Auth::user()->group_user_id) == "2") {
            $storeAdmin = Store::where('bu_id', \Auth::user()->bu_id)->get();

            foreach ($storeAdmin as $key => $value) {
                array_push($storeid, $value->store_id);
            }
        }

        // dd($type." ".$report." ".$store);

        if ($report == 'tredeem') {

            $title = 'Report'; // Report title

            $meta = [ // For displaying filters description on header
                'Report Name' => 'Transection Redeem',
                'Date' => $start_end,
            ];

            $queryBuilder;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($store == 'all') {
                        $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->orderBy('redeem_id');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $store)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"]) // Do some querying..
                            ->orderBy('redeem_id');
                    }
                } else {

                    if ($store == 'all') {
                        $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', \Auth::user()->store_id)->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n") // Do some querying..
                            ->orderBy('redeem_id');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', \Auth::user()->store_id)->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $store)->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->orderBy('redeem_id');
                    }
                }

            } else {
                if ($store == 'all') {
                    $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"]) // Do some querying..
                        ->orderBy('redeem_id');

                    // dd($queryBuilder->get());
                } else {
                    $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $store)->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"]) // Do some querying..
                        ->orderBy('redeem_id');
                }
            }

            $columns = [ // Set Column to be displayed
                'Date Time' => 'create_date',
                'Branch No' => function ($value) {
                    return $value['Store']->store_id;
                },
                'Branch Name' => function ($value) {
                    return $value['Store']->store_name;
                },
                'Maxcard No' => 'maxcard_id',
                'Redeem Code' => function ($value) {
                    return $value['Redeem']->rd_code;
                },
                'Redeem Desc' => function ($value) {
                    return $value['Redeem']->rd_desc;
                },
                'Point' => function ($value) {
                    return $value['Redeem']->rd_point;
                },
                // 'Total Balance' => 'balance',
                // 'Status' => function($result) { // You can do if statement or any action do you want inside this closure
                //     return ($result->balance > 100000) ? 'Rich Man' : 'Normal Guy';
                // }
            ];

            // Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

            $namefile = 'Transection_Redeem_' . Carbon::now()->format('Y-m-d');

            switch ($type) {
                case 'pdf':
                    // dd('pdf');
                    return PdfReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)
                        ->showTotal([
                            'Point' => 'point',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'excel':
                    return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)
                        ->showTotal([
                            'Point' => 'point',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'csv':
                    return CSVReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)
                        ->showTotal([
                            'Point' => 'point',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
            }

        }

        //////////////////////////////// Summary Redeem

        if ($report == 'sredeem') {

            $title = 'Report'; // Report title

            $meta = [ // For displaying filters description on header
                'Report Name' => 'Summary Redeem',
                'Date' => $start_end,
            ];

            $queryBuilder;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($store == 'all') {
                        $queryBuilder = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->whereIn('transections_redeem.store_id', $storeid)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');

                        // $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        //     ->orderBy('redeem_id');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        // ->whereIn('transections_redeem.store_id', $storeid)
                            ->where('transections_redeem.store_id', $store)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');
                        // $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $store)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"]) // Do some querying..
                        //     ->orderBy('redeem_id');
                    }
                } else {

                    if ($store == 'all') {

                        $queryBuilder = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        // ->whereIn('transections_redeem.store_id', $storeid)
                            ->where('transections_redeem.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');

                        // $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', \Auth::user())->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n") // Do some querying..
                        //     ->orderBy('redeem_id');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                            ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                            ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transections_redeem.store_id', '!=', null)
                            ->where('transections_redeem.rd_id', '!=', null)
                            ->where('transections_redeem.void_status', '=', "n")
                            ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        // ->whereIn('transections_redeem.store_id', $storeid)
                            ->where('transections_redeem.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');
                    }
                }

            } else {
                if ($store == 'all') {
                    $queryBuilder = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                        ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                        ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transections_redeem.store_id', '!=', null)
                        ->where('transections_redeem.rd_id', '!=', null)
                        ->where('transections_redeem.void_status', '=', "n")
                        ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                    // ->whereIn('transections_redeem.store_id', $storeid)
                    //->where('transections_redeem.store_id', \Auth::user())
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->orderBy('store.store_id');

                    // dd($queryBuilder->get());
                } else {

                    $queryBuilder = TranSectionRedeem::join('redeem_master', 'redeem_master.rd_id', '=', 'transections_redeem.rd_id')
                        ->join('store', 'store.store_id', '=', 'transections_redeem.store_id')
                        ->select(DB::raw('SUM(redeem_master.rd_point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transections_redeem.store_id', '!=', null)
                        ->where('transections_redeem.rd_id', '!=', null)
                        ->where('transections_redeem.void_status', '=', "n")
                        ->whereBetween('transections_redeem.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                    // ->whereIn('transections_redeem.store_id', $storeid)
                    //->where('transections_redeem.store_id', \Auth::user())
                        ->where('transections_redeem.store_id', $store)
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->orderBy('store.store_id');
                }
            }

            $columns = [ // Set Column to be displayed
                'Branch No' => function ($value) {
                    return $value['Store']->store_id;
                },
                'Branch Name' => function ($value) {
                    return $value['Store']->store_name;
                },
                'Total point' => 'total',

                // 'Total Balance' => 'balance',
                // 'Status' => function($result) { // You can do if statement or any action do you want inside this closure
                //     return ($result->balance > 100000) ? 'Rich Man' : 'Normal Guy';
                // }
            ];

            // Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

            $namefile = 'Summary_Redeem_' . Carbon::now()->format('Y-m-d');

            switch ($type) {
                case 'pdf':
                    // dd('pdf');
                    return PdfReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showTotal([
                            'Total point' => 'total',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'excel':
                    return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showTotal([
                            'Total point' => 'total',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'csv':
                    return CSVReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showTotal([
                            'Total point' => 'total',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
            }

        }

        // earn header

        if ($report == 'tearn') {

            $title = 'Report'; // Report title

            $meta = [ // For displaying filters description on header
                'Report Name' => 'Transection Earn Header',
                'Date' => $start_end,
            ];

            $queryBuilder;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($store == 'all') {
                        $queryBuilder = TransectionHeader::with(['Store'])->where('store_id', '!=', null)->where('maxcard_id', 'like', '%' . $maxcard . '%')->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->orderBy('create_date');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TransectionHeader::with(['Store'])->where('store_id', '!=', null)->where('maxcard_id', 'like', '%' . $maxcard . '%')->where('store_id', $request->input('store'))->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->orderBy('create_date');
                    }
                } else {

                    if ($store == 'all') {
                        $queryBuilder = TransectionHeader::with(['Store'])->where('store_id', \Auth::user()->store_id)->where('maxcard_id', 'like', '%' . $maxcard . '%')->where('store_id', '!=', null)->where('void_status', '=', "n")
                            ->orderBy('create_date');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TransectionHeader::with(['Store'])->where('store_id', \Auth::user()->store_id)->where('maxcard_id', 'like', '%' . $maxcard . '%')->where('store_id', '!=', null)->where('store_id', $request->input('store'))->where('void_status', '=', "n")->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->orderBy('create_date');
                    }
                }

            } else {
                if ($store == 'all') {
                    $queryBuilder = TransectionHeader::with(['Store'])->where('store_id', '!=', null)->where('void_status', '=', "n")->where('maxcard_id', 'like', '%' . $maxcard . '%')->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->orderBy('create_date');

                    // dd($queryBuilder->get());
                } else {
                    $queryBuilder = TransectionHeader::with(['Store'])->where('store_id', '!=', null)->where('maxcard_id', 'like', '%' . $maxcard . '%')->where('store_id', $request->input('store'))->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])->where('void_status', '=', "n")
                        ->orderBy('create_date');
                }
            }

            $columns = [ // Set Column to be displayed
                'Trans ID' => 'id_trans',
                'Date Time' => 'create_date',
                'Branch No' => function ($value) {
                    return $value['Store']->store_id;
                },
                'Branch Name' => function ($value) {
                    return $value['Store']->store_name;
                },
                'Maxcard No' => 'maxcard_id',
                'Warranty No.' => 'car_insurance_no',
                'Car Model' => 'car_generation',
                'Car License' => 'car_license',
                'Chassis No.' => 'car_chassis_no',
                'Film Price' => 'total_film',
                'Sum' => 'total_price',
                'Discount' => 'discount',
                'Total' => 'sum_price',
                'Point' => 'point',

                // 'Total Balance' => 'balance',
                // 'Status' => function($result) { // You can do if statement or any action do you want inside this closure
                //     return ($result->balance > 100000) ? 'Rich Man' : 'Normal Guy';
                // }
            ];

            // Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

            $namefile = 'Transection_Earn_Header_' . Carbon::now()->format('Y-m-d');

            switch ($type) {
                case 'pdf':
                    // dd('pdf');
                    return PdfReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)
                        ->showTotal([
                            'Point' => 'point',
                            'Total' => '',
                        ])
                        ->setPaper('a4')
                        ->setOrientation('landscape')

                        ->limit(20)

                        ->download($namefile);
                case 'excel':
                    return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)
                        ->showTotal([
                            'Point' => 'point',
                            'Total' => '',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'csv':
                    return CSVReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)
                        ->showTotal([
                            'Point' => 'point',
                            'Total' => '',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
            }

        }

        if ($report == 'dearn') {

            $title = 'Report'; // Report title

            $meta = [ // For displaying filters description on header
                'Report Name' => 'Transaction Earn Detail',
                'Date' => $start_end,
            ];

            $queryBuilder;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($store == 'all') {
                        $queryBuilder = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->where('transection_header.maxcard_id', 'like', '%' . $maxcard . '%')
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->whereIn('transection_header.store_id', $storeid)
                            ->orderBy('transection_detail.create_date');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.maxcard_id', 'like', '%' . $maxcard . '%')
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', $store)
                            ->orderBy('transection_detail.create_date');
                    }
                } else {

                    if ($store == 'all') {
                        $queryBuilder = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.maxcard_id', 'like', '%' . $maxcard . '%')
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->orderBy('transection_detail.create_date');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                            ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                            ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                            ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.maxcard_id', 'like', '%' . $maxcard . '%')
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->orderBy('transection_detail.create_date');
                    }
                }

            } else {
                if ($store == 'all') {
                    $queryBuilder = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                        ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                        ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                        ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.maxcard_id', 'like', '%' . $maxcard . '%')
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->orderBy('transection_detail.create_date');

                    // dd($queryBuilder->get());
                } else {
                    $queryBuilder = TransectionDetail::join('transection_header', 'transection_header.id_trans', '=', 'transection_detail.id_trans')
                        ->join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->join('product', 'product.pid', '=', 'transection_detail.product_id')
                        ->join('type_product', 'type_product.type_product_id', '=', 'product.type_product_id')
                        ->select('transection_detail.*', 'product.pid', 'product.p_name', 'store.store_id', 'store.store_name', 'type_product.type_product_name', 'transection_header.id_trans', 'transection_header.maxcard_id')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.maxcard_id', 'like', '%' . $maxcard . '%')
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_detail.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->where('transection_header.store_id', $store)
                        ->orderBy('transection_detail.create_date');
                }
            }

            $columns = [ // Set Column to be displayed
                'Trans ID' => 'id_trans',
                'Date Time' => 'create_date',
                'Branch No' => function ($value) {
                    return $value->store_id;
                },
                'Branch Name' => function ($value) {
                    return $value->store_name;
                },
                'Maxcard No' => 'maxcard_id',
                'Warranty No.' => 'car_insurance_no',
                'Product ID' => 'pid',
                'Product Name' => 'p_name',
                'Type' => 'type_product_name',
                'Price' => 'price',

                // 'Total Balance' => 'balance',
                // 'Status' => function($result) { // You can do if statement or any action do you want inside this closure
                //     return ($result->balance > 100000) ? 'Rich Man' : 'Normal Guy';
                // }
            ];

            // Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

            $namefile = 'Transection_Earn_Detail_' . Carbon::now()->format('Y-m-d');

            switch ($type) {
                case 'pdf':
                    // dd('pdf');
                    return PdfReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)

                        ->setPaper('a4')
                        ->setOrientation('landscape')

                        ->limit(20)

                        ->download($namefile);
                case 'excel':
                    return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)

                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'csv':
                    return CSVReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showNumColumn(false)

                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
            }

        }

        //////////////////////////////// Summary Earn

        if ($report == 'searn') {

            $title = 'Report'; // Report title

            $meta = [ // For displaying filters description on header
                'Report Name' => 'Summary Earn',
                'Date' => $start_end,
            ];

            $queryBuilder;

            if (trim(\Auth::user()->group_user_id) != "1") {
                if (trim(\Auth::user()->group_user_id) == "2") {
                    if ($store == 'all') {
                        $queryBuilder = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->whereIn('transection_header.store_id', $storeid)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');

                        // $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        //     ->orderBy('redeem_id');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', $request->input('store'))
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');
                        // $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('store_id', $store)->where('void_status', '=', "n")->whereIn('store_id', $storeid)->whereBetween('create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"]) // Do some querying..
                        //     ->orderBy('redeem_id');
                    }
                } else {

                    if ($store == 'all') {

                        $queryBuilder = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');

                        // $queryBuilder = TranSectionRedeem::with(['Redeem', 'Store'])->where('store_id', \Auth::user())->where('store_id', '!=', null)->where('rd_id', '!=', null)->where('void_status', '=', "n") // Do some querying..
                        //     ->orderBy('redeem_id');

                        // dd($queryBuilder->get());
                    } else {
                        $queryBuilder = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                            ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                            ->where('transection_header.store_id', '!=', null)
                            ->where('transection_header.void_status', '=', "n")
                            ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                            ->where('transection_header.store_id', \Auth::user()->store_id)
                            ->groupBy('store.store_id')
                            ->groupBy('store.store_name')
                            ->orderBy('store.store_id');
                    }
                }

            } else {
                if ($store == 'all') {
                    $queryBuilder = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->orderBy('store.store_id');

                    // dd($queryBuilder->get());
                } else {

                    $queryBuilder = TransectionHeader::join('store', 'store.store_id', '=', 'transection_header.store_id')
                        ->select(DB::raw('SUM(transection_header.point) as total'), 'store.store_id', 'store.store_name')
                        ->where('transection_header.store_id', '!=', null)
                        ->where('transection_header.void_status', '=', "n")
                        ->whereBetween('transection_header.create_date', [$splitTime[0] . " 00:00:00", $splitTime[1] . " 23:59:59"])
                        ->where('transection_header.store_id', $request->input('store'))
                        ->groupBy('store.store_id')
                        ->groupBy('store.store_name')
                        ->orderBy('store.store_id');
                }
            }

            $columns = [ // Set Column to be displayed
                'Branch No' => function ($value) {
                    return $value->store_id;
                },
                'Branch Name' => function ($value) {
                    return $value->store_name;
                },
                'Total point' => 'total',

                // 'Total Balance' => 'balance',
                // 'Status' => function($result) { // You can do if statement or any action do you want inside this closure
                //     return ($result->balance > 100000) ? 'Rich Man' : 'Normal Guy';
                // }
            ];

            // Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

            $namefile = 'Summary_Earn_' . Carbon::now()->format('Y-m-d');

            switch ($type) {
                case 'pdf':
                    // dd('pdf');
                    return PdfReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showTotal([
                            'Total point' => '',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'excel':
                    return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showTotal([
                            'Total point' => '',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
                case 'csv':
                    return CSVReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Date Time', [
                            'displayAs' => function ($result) {

                                return Carbon::parse($result->create_date)->format('Y-m-d');
                            },
                            'class' => 'left',
                        ])
                        ->setCss([
                            '.head-content' => 'border-width: 1px',
                        ])
                        ->showTotal([
                            'Total point' => '',
                        ])
                        ->setPaper('a4')

                        ->limit(20)

                        ->download($namefile);
            }

        }

    }

}
