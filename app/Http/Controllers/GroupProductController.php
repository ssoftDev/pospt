<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\GroupProduct;
use App\Models\GroupTemplateProduct;
use App\Models\Module;
use App\Models\Store;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;



class GroupProductController extends Controller
{
    //

    public $module = 'manage/group-product';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $groupProduct = new GroupProduct;

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) != "1") {

            if (!empty($input['q'])) {
                $groupProduct = $groupProduct->where('gp_name', 'like', '%' . trim($input['q']) . '%')->where('bu_id', trim(\Auth::user()->bu_id))->with(['BusinessUnit', 'GroupProductTemplate'])->orderBy('create_date', 'desc')->paginate(10);
            } else {
                $groupProduct = $groupProduct->where('bu_id', trim(\Auth::user()->bu_id))->with(['BusinessUnit', 'GroupProductTemplate'])->orderBy('create_date', 'desc')->paginate(10);
            }

        } else {
            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                  if (!empty($input['q'])) {
                    $groupProduct = $groupProduct->where('gp_name', 'like', '%' . trim($input['q']) . '%')->where('bu_id', $request->session()->get('bu'))->with(['BusinessUnit', 'GroupProductTemplate'])->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $groupProduct = $groupProduct->with(['BusinessUnit', 'GroupProductTemplate'])->where('bu_id', $request->session()->get('bu'))->orderBy('create_date', 'desc')->paginate(10);
                }
            } else {
                if (!empty($input['q'])) {
                    $groupProduct = $groupProduct->where('gp_name', 'like', '%' . trim($input['q']) . '%')->with(['BusinessUnit', 'GroupProductTemplate'])->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $groupProduct = $groupProduct->with(['BusinessUnit', 'GroupProductTemplate'])->orderBy('create_date', 'desc')->paginate(10);
                }

            }

        }

        return view('manage.groupProduct.groupProduct', compact('groupProduct'));
    }

    public function create(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;
        $groupTemplateProduct = new GroupTemplateProduct;

        if (trim(\Auth::user()->group_user_id) != "1") {

            // $store = new Store;
            // $store = $store->where('store_id',\Auth::user()->store_id)->with('BusinessUnit')->first();

            $businessUnit = $businessUnit->where('bu_id', trim(\Auth::user()->bu_id))->get();

            $groupTemplateProduct = $groupTemplateProduct->where('bu_id', trim(\Auth::user()->bu_id))->orderBy('create_date', 'desc')->get();


        } else {
            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                $businessUnit = $businessUnit->where('bu_id', $request->session()->get('bu'))->get();
                $groupTemplateProduct = $groupTemplateProduct->where('bu_id', $request->session()->get('bu'))->orderBy('create_date', 'desc')->get();

            } else {
                $businessUnit = $businessUnit->get();
                $groupTemplateProduct = $groupTemplateProduct->orderBy('create_date', 'desc')->get();
            }

        }

        // dd($request->session()->get('bu') );

        return view('manage.groupProduct.formgroupProduct', compact(['businessUnit', 'groupTemplateProduct']));
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $groupProduct = new GroupProduct();

        $edit = $groupProduct->where('gp_id', $id)->get();

        $businessUnit = new BusinessUnit;
        $businessUnit = $businessUnit->get();

        $groupTemplateProduct = new GroupTemplateProduct;
        $groupTemplateProduct = $groupTemplateProduct->get();

        return view('manage.groupProduct.formgroupProduct', compact(['edit', 'businessUnit', 'groupTemplateProduct']));
    }

    public function store(Request $request)
    {

        $groupProduct = new GroupProduct;

         $validator = Validator::make($request->all(), [
            'gt_product' => 'required',
            'businessunit' => 'required',
        ]);

         if ($validator->fails()) {
            return redirect('manage/group-product/getform')
                ->withErrors($validator)
                ->withInput();
        }

        //    dd($request->all());
        else{
            $groupProduct->gp_name = $request->input('g_name');
            $groupProduct->gp_id = ($groupProduct->max('gp_id') + 1) . '';
            $groupProduct->gt_id = $request->input('gt_product');
            $groupProduct->bu_id = $request->input('businessunit');
            $groupProduct->is_enable = $request->input('status');
            $groupProduct->create_by = \Auth::user()->user;
            $groupProduct->create_date = Carbon::now();
            $groupProduct->save();

        }




        //  dd($request->all());
        $groupProduct->save();
        flashMe()->success();
        return redirect('manage/group-product');
    }

    public function update(Request $request)
    {
        $groupProduct = new GroupProduct();

        $update['gp_name'] = $request->input('g_name');
        $update['gt_id'] = $request->input('gt_product');
        // $update['bu_id'] = $request->input('businessunit');
        $update['is_enable'] = $request->input('status');
        $update['update_by'] = \Auth::user()->user;
$update['update_date'] = Carbon::now();



        $groupProduct->where('gp_id', $request->input('id'))
            ->update($update);

        flashMe()->success();
        return redirect('manage/group-product');
    }

    public function destroy(Request $request)
    {
        //  dd($id);

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $groupProduct = new GroupProduct();
        $groupProduct->where('gp_id', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/group-product');
    }
}
