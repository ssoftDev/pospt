<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\GroupProduct;
use App\Models\Module;
use App\Models\MP_Product;
use App\Models\MP_Product_Template;
use App\Models\Store;
use App\Models\TemplateProduct;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;


class GroupTemplateProductController extends Controller
{
    //

    public $module = 'manage/group-template-product';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $TemplateProduct = new TemplateProduct;

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) != "1") {

            if (!empty($input['q'])) {
                $TemplateProduct = $TemplateProduct->where('bu_id', trim(\Auth::user()->bu_id))->where('gt_name', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
            } else {
                $TemplateProduct = $TemplateProduct->where('bu_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
            }

        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                if (!empty($input['q'])) {
                    $TemplateProduct = $TemplateProduct->where('bu_id', $request->session()->get('bu'))->where('gt_name', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $TemplateProduct = $TemplateProduct->where('bu_id', $request->session()->get('bu'))->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                }

            } else {
                if (!empty($input['q'])) {
                    $TemplateProduct = $TemplateProduct->where('gt_name', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $TemplateProduct = $TemplateProduct->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                }

            }

        }

        return view('manage.groupTemplateProduct.groupTemplateProduct', compact('TemplateProduct'));
    }

    public function create(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;

        if (trim(\Auth::user()->group_user_id) != "1") {

            $store = new Store;
            $store = $store->where('store_id', \Auth::user()->store_id)->with('BusinessUnit')->first();

            $businessUnit = $businessUnit->where('bu_id', trim(\Auth::user()->bu_id))->get();

            // dd($store[0]->bu_id);
        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                $businessUnit = $businessUnit->where('bu_id', $request->session()->get('bu'))->get();

            } else {

                $businessUnit = $businessUnit->get();
            }

        }

        return view('manage.groupTemplateProduct.formgroupTemplateProduct', compact('businessUnit'));
    }

    public function store(Request $request)
    {

        //dd($request->all());

        $TemplateProduct = new TemplateProduct;

        $TemplateProduct->gt_id = ($TemplateProduct->max('gt_id') + 1) . '';

        $TemplateProduct->gt_name = $request->input('t_name');

        $TemplateProduct->bu_id = $request->input('businessunit');

        $TemplateProduct->create_date = Carbon::now();

        $TemplateProduct->create_by = \Auth::user()->user;

        $GroupSeq = json_decode($request->input('dataTableGroup'));

        if ($GroupSeq != null) {

            foreach ($GroupSeq as $key => $value) {

                DB::table('mp_template_product')->insert([
                    ['gt_id' => $TemplateProduct->gt_id,
                        'gp_id' => $value->id,
                        'seq' => $key + 1,
                    ],
                ]);

            }

        }

        $TemplateProduct->save();

        flashMe()->success();

        return redirect('manage/group-template-product');
    }

    public function update(Request $request)
    {

        $TemplateProduct = new TemplateProduct;

        $updateTemplateProduct['gt_name'] = $request->input('t_name');

        $updateTemplateProduct['update_date'] = Carbon::now();

        $updateTemplateProduct['update_by'] = \Auth::user()->user;

        // $updateTemplateProduct['bu_id'] = $request->input('businessunit');

        $GroupSeq = json_decode($request->input('dataTableGroup'));

        if ($GroupSeq != null) {

            $MP_Product_Template = new MP_Product_Template;

            $MP_Product_Template->where('gt_id', $request->input('id'))->delete();

            foreach ($GroupSeq as $key => $value) {

                DB::table('mp_template_product')->insert([
                    ['gt_id' => $request->input('id'),
                        'gp_id' => $value->id,
                        'seq' => $key + 1,
                    ],
                ]);

            }

        } else {
            $MP_Product_Template = new MP_Product_Template;

            $MP_Product_Template->where('gt_id', $request->input('id'))->delete();
        }

        $TemplateProduct->where('gt_id', $request->input('id'))
            ->update($updateTemplateProduct);

        flashMe()->success();

        return redirect('manage/group-template-product');
    }

    public function edit(Request $request, $id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $TemplateProduct = new TemplateProduct;

        $MP_Product_Template = new MP_Product_Template;

        $edit = $TemplateProduct->where('gt_id', $id)->get();

        $mpProductTem = $MP_Product_Template->where('gt_id', $id)->orderBy('seq', 'ASC')->get();

        $businessUnit = new BusinessUnit;

        if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
            $businessUnit = $businessUnit->where('bu_id', $request->session()->get('bu'))->get();

        } else {

            $businessUnit = $businessUnit->get();
        }

        $groupProduct = new GroupProduct;
        $groupProduct = $groupProduct->get();

        return view('manage.groupTemplateProduct.formgroupTemplateProduct', compact(['edit', 'mpProductTem', 'businessUnit', 'groupProduct']));
    }

    public function getGroupProduct(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');

            $dataTable = $request->get('data');
            $idData = [];

            if ($dataTable != null) {
                foreach ($dataTable as $key => $value) {
                    array_push($idData, $value['id']);
                }
            }

            $groupProduct = new GroupProduct;
            $data = $groupProduct
                ->where('bu_id', $query)
                ->WhereNotIn('gp_id', $idData)
                ->get();
            $output = '';
            foreach ($data as $row) {
                $output .= '<option value="' . $row->gp_id . '"' . 'name="' . $row->gp_name . '"' . '>' . $row->gp_name . '</option>';
            }
            $output .= '</ul>';
            echo $output;
        }
        // return response()->json([
        //     'dataTable' =>  $idData
        // ]);
    }

    public function getProduct(Request $request)
    {
        if ($request->ajax()) {

            $query = $request->get('query');

            $MP_Product = new MP_Product;

            $data = $MP_Product
                ->where('gp_id', $query)
                ->with('Product')
                ->get();

            // $output .= '</ul>';
            return response()->json(['data' => $data]);
        }
    }

    public function destroy(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }
        //  dd($id);

        $TemplateProduct = new TemplateProduct();
        $TemplateProduct->where('gt_id', $request->input('iddelete'))->delete();

        $MP_Product_Template = new MP_Product_Template;

        $MP_Product_Template->where('gt_id', $request->input('iddelete'))->delete();

        flashMe()->success();
        return redirect('manage/group-template-product');
    }

}
