<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\Module;
use Illuminate\Http\Request;
use Session;

class BusinessUnitController extends Controller
{
    //

    public $module = 'manage/business-unit';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) != "1") {

            if (!empty($input['q'])) {
                $businessUnit = $businessUnit->where('bu_name', 'like', '%' . trim($input['q']) . '%')->where('bu_id',trim(\Auth::user()->bu_id))->paginate(10);
            } else {
                $businessUnit = $businessUnit->where('bu_id',trim(\Auth::user()->bu_id))->paginate(10);
            }
        } else {
            if (!empty($input['q'])) {
                $businessUnit = $businessUnit->where('bu_name', 'like', '%' . trim($input['q']) . '%')->paginate(10);
            } else {
                $businessUnit = $businessUnit->paginate(10);
            }
        }

        return view('manage.businessunit.business', compact('businessUnit'));
    }

    // public function edit($id){

    //     $businessUnit = new BusinessUnit();

    //     $edit = $businessUnit->where('bu_id',$id)->get();

    //     return view('manage.user.formuser', ['edit'=>$edit]);
    // }

    public function SetBu_Session(Request $request)
    {
        if ($request->ajax()) {
            $bu = $request->get('bu_id');

            //   $dataTable = $request->get('dataTable');

             Session::put('bu', $bu);


            return response()->json(['session' => $request->session()->get('bu')]);

        }

    }

    public function update(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit();

        $update['bu_name'] = $request->input('bu_name_edit');

        $businessUnit->where('bu_id', $request->input('id'))
            ->update($update);
        flashMe()->success();
        return redirect('manage/business-unit');
    }

    public function store(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;

        //    dd($request->all());

        $businessUnit->bu_name = $request->input('bu_name');
        $businessUnit->bu_id = ($businessUnit->max('bu_id') + 1) . '';

        $businessUnit->save();
        flashMe()->success();
        return redirect('manage/business-unit');
    }

    public function destroy(Request $request)
    {
        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit();
        $businessUnit->where('bu_id', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/business-unit');
    }

}
