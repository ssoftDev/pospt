<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\Module;
use App\Models\MP_Earn;
use App\Models\Product;
use App\Models\SetupEarn;
use App\Models\Store;
use Carbon\Carbon;
use DateTime;
use DB;
use Illuminate\Http\Request;

class EarnController extends Controller
{
    //

    public $module = 'manage/setup-earn';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $SetupEarn = new SetupEarn;

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) != "1") {

            if (!empty($input['q'])) {
                $SetupEarn = $SetupEarn->where('bu_id', trim(\Auth::user()->bu_id))->select('bu_id')->with(['BusinessUnit' => function ($query) use ($input) {
                    $query->where('bu_name', 'like', '%' . trim($input['q']) . '%');
                }])->groupBy('bu_id')->orderBy('bu_id', 'desc')->paginate(10);
            } else {

                $SetupEarn = $SetupEarn->where('bu_id', trim(\Auth::user()->bu_id))->select('bu_id')->groupBy('bu_id')->with('BusinessUnit')->orderBy('bu_id', 'desc')->paginate(10);

            }

        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {

                if (!empty($input['q'])) {
                    $SetupEarn = $SetupEarn->select('bu_id')->where('bu_id',$request->session()->get('bu'))->with(['BusinessUnit' => function ($query) use ($input) {
                        $query->where('bu_name', 'like', '%' . trim($input['q']) . '%');
                    }])->groupBy('bu_id')->orderBy('bu_id', 'desc')->paginate(10);
                } else {

                    $SetupEarn = $SetupEarn->select('bu_id')->where('bu_id',$request->session()->get('bu'))->groupBy('bu_id')->with('BusinessUnit')->orderBy('bu_id', 'desc')->paginate(10);

                }
            } else {

                if (!empty($input['q'])) {
                    $SetupEarn = $SetupEarn->select('bu_id')->with(['BusinessUnit' => function ($query) use ($input) {
                        $query->where('bu_name', 'like', '%' . trim($input['q']) . '%');
                    }])->groupBy('bu_id')->orderBy('bu_id', 'desc')->paginate(10);
                } else {

                    $SetupEarn = $SetupEarn->select('bu_id')->groupBy('bu_id')->with('BusinessUnit')->orderBy('bu_id', 'desc')->paginate(10);

                }
            }

        }

        return view('manage.setupEarn.earn', compact('SetupEarn'));
    }

    public function store(Request $request)
    {

        $tableEarn = json_decode($request->input('tableearn'));

        if ($tableEarn != null) {

            foreach ($tableEarn as $key => $value) {

                $id = DB::table('setup_earn')->max('eid') + 1;
                // dd($id);
                $splitTime = explode(" - ", $value->start_end);
                //  dd($splitTime);

                $start_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[0]));
                $end_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[1]));

                DB::table('setup_earn')->insert([
                    ['eid' => $id,
                        'bu_id' => $value->bu_id,
                        'seq' => $key + 1,
                        'bath' => $value->amount,
                        'point' => $value->per_point,
                        'multiply' => $value->multiply,
                        'is_check' => $value->sku,
                        'start_date' => $start_time,
                        'expire_date' => $end_time,
                        'create_date' => Carbon::now(),
                    ],
                ]);

                if ($value->sku == 'y') {
                    DB::table('mp_earn_product')->insert([
                        ['eid' => $id,
                            'pid' => $value->pid,
                            'create_date' => Carbon::now(),
                        ],
                    ]);
                }

            }

        } else {
            return redirect()->back()->withErrors(['ต้องมีข้อมูลอย่างน้อย 1 ตัว']);
        }

        // dd($tableEarn);

        // $TemplateProduct = new TemplateProduct;

        // $TemplateProduct->tmppd_id = ($TemplateProduct->max('tmppd_id')+1).'';

        // $TemplateProduct->tmppd_name = $request->input('t_name');

        // $TemplateProduct->bu_id = $request->input('businessunit');

        // $GroupSeq = json_decode($request->input('dataTableGroup'));

        // if($GroupSeq!=null){

        //     foreach($GroupSeq as $key=>$value){

        //         DB::table('mp_template_product')->insert([
        //             ['tmppd_id' => $TemplateProduct->tmppd_id,
        //             'gp_id' => $value->id,
        //             'seq' => $key+1
        //             ]
        //         ]);

        //     }

        // }

        // $TemplateProduct->save();
        flashMe()->success();
        return redirect('manage/setup-earn');
    }

    public function update(Request $request)
    {

        // $MP_Earn = new MP_Earn;

        // $MP_Earn->where('eid',$request->input('id'))->delete();

        $eid = DB::table('setup_earn')->select('eid')->where('bu_id', $request->input('id'))->get();

        foreach ($eid as $key => $value) {
            DB::table('mp_earn_product')->where('eid', $value->eid)->delete();
        }

        $SetupEarn = new SetupEarn;

        $SetupEarn->where('bu_id', $request->input('id'))->delete();

        $tableEarn = json_decode($request->input('tableearn'));

        if ($tableEarn != null) {

            foreach ($tableEarn as $key => $value) {

                $id = DB::table('setup_earn')->max('eid') + 1;
                $splitTime = explode(" - ", $value->start_end);

                $start_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[0]));
                $end_time = new DateTime(preg_replace('~\x{00a0}~u', ' ', $splitTime[1]));

                // dd( $start_time);

                DB::table('setup_earn')->insert([
                    ['eid' => $id,
                        'bu_id' => $value->bu_id,
                        'seq' => $key + 1,
                        'bath' => $value->amount,
                        'point' => $value->per_point,
                        'multiply' => $value->multiply,
                        'is_check' => $value->sku,
                        'start_date' => $start_time,
                        'expire_date' => $end_time,
                        'create_date' => Carbon::now(),
                    ],
                ]);

                if ($value->sku == 'y') {
                    DB::table('mp_earn_product')->insert([
                        ['eid' => $id,
                            'pid' => $value->pid,
                            'create_date' => Carbon::now(),
                        ],
                    ]);
                }

            }

        }
        flashMe()->success();
        return redirect('manage/setup-earn');
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $SetupEarn = new SetupEarn;

        $MP_Earn = new MP_Earn;

        $MP_Earn = $MP_Earn->get();

        $edit = $SetupEarn->where('bu_id', $id)->orderBy('seq', 'ASC')->get();

        $businessUnit = new BusinessUnit;
        $businessUnit = $businessUnit->get();

        $Product = new Product;
        $Product = $Product->get();

        return view('manage.setupEarn.formEarn', compact(['edit', 'businessUnit', 'MP_Earn', 'id']));
    }

    public function getEarn(Request $request)
    {
        $query = $request->get('query');

        $SetupEarn = new SetupEarn;

        $SetupEarn = $SetupEarn->where('bu_id', $query)->with('MP')->orderBy('seq', 'ASC')->get();

        // $output .= '</ul>';
        return response()->json(['data' => $SetupEarn]);
    }

    public function create()
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $SetupEarn = new SetupEarn;

        $businessUnit = new BusinessUnit;

        $product = null;

        if (trim(\Auth::user()->group_user_id) != "1") {
            // $store = new Store;
            // $store = $store->where('store_id', \Auth::user()->store_id)->with('BusinessUnit')->first();

            $SetupEarn = $SetupEarn->where('bu_id', \Auth::user()->bu_id)->select('bu_id')->groupBy('bu_id')->with('BusinessUnit')->get();


            if (count($SetupEarn) == 0) {
                $businessUnit = $businessUnit->where('bu_id', \Auth::user()->bu_id)->get();

                

            } else {
                $businessUnit = [];
            }

        } else {
            $SetupEarn = $SetupEarn->select('bu_id')->groupBy('bu_id')->with('BusinessUnit')->get();

            $bu_id = [];

            foreach ($SetupEarn as $key => $value) {
                array_push($bu_id, $value->bu_id);
            }

            $businessUnit = $businessUnit->WhereNotIn('bu_id', $bu_id)->get();
        }

        //    ; dd($bu_id);

        return view('manage.setupEarn.formEarn', compact('businessUnit'));
    }

    public function getProduct(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');

            //   $dataTable = $request->get('dataTable');

            $Product = new Product;
            $data = $Product
                ->where('p_name', 'LIKE', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row) {
                $output .= '
       <li id="' . $row->pid . '"' . '><a href="#">' . $row->p_name . '</a></li>
       ';
            }
            $output .= '</ul>';
            echo $output;
        }

    }

    public function destroy(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $eid = DB::table('setup_earn')->select('eid')->where('bu_id', $request->input('iddelete'))->get();

        foreach ($eid as $key => $value) {
            DB::table('mp_earn_product')->where('eid', $value->eid)->delete();
        }

        $SetupEarn = new SetupEarn;

        $SetupEarn->where('bu_id', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/setup-earn');
    }

}
