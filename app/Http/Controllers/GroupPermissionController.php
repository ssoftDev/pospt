<?php

namespace App\Http\Controllers;

use App\Models\GroupPermission;
use App\Models\GroupUser;
use App\Models\Module;
use Illuminate\Http\Request;
use Carbon\Carbon;


class GroupPermissionController extends Controller
{
    //

    public $module = 'manage/permission';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $GroupPermission = new GroupPermission;

        $input = $request->all();

        if (!empty($input['u'])) {
            $GroupPermission = $GroupPermission->where('group_user_id',trim($input['u']))->orderBy('create_date', 'desc')->paginate(10);
        } else {

            $GroupPermission = $GroupPermission->orderBy('create_date', 'desc')->paginate(10);

        }

        return view('manage.groupPermission.groupPermission', compact('GroupPermission'));
    }

    public function store(Request $request)
    {

        // dd($request->all());

        $row = \DB::table('group_permission')->where('module_id', $request->input('module'))
            ->where('group_user_id', $request->input('group_user'))
            ->get();

        if (count($row) == 0) {
            $GroupPermission = new GroupPermission;

            $permission_to_json = array();

            $GroupPermission->id = ($GroupPermission->max('id') + 1) . '';
            $GroupPermission->group_user_id = $request->input('group_user');
            $GroupPermission->module_id = $request->input('module');
            $GroupPermission->create_date = Carbon::now();
            $GroupPermission->create_by = \Auth::user()->user;




            $permission_to_json['is_view'] = $request->input('is_view') == null ? 0 : 1;
            $permission_to_json['is_add'] = $request->input('is_add') == null ? 0 : 1;
            $permission_to_json['is_edit'] = $request->input('is_edit') == null ? 0 : 1;
            $permission_to_json['is_remove'] = $request->input('is_remove') == null ? 0 : 1;

            $GroupPermission->access_data = json_encode($permission_to_json);
            $GroupPermission->save();

            flashMe()->success();
            return redirect('manage/permission');

        } else {

            return redirect()->back()->withErrors(['Module และ Group ซ้ำในระบบ']);
        }

    }

    public function update(Request $request)
    {

        $GroupPermission = new GroupPermission;

        $permission_to_json = array();

        // $UpdateGroupPermission['group_user_id'] = $request->input('group_user');
        // $UpdateGroupPermission['module_id'] = $request->input('module');

        $permission_to_json['is_view'] = $request->input('is_view') == null ? 0 : 1;
        $permission_to_json['is_add'] = $request->input('is_add') == null ? 0 : 1;
        $permission_to_json['is_edit'] = $request->input('is_edit') == null ? 0 : 1;
        $permission_to_json['is_remove'] = $request->input('is_remove') == null ? 0 : 1;

        // dd(json_encode($permission_to_json));

        $UpdateGroupPermission['access_data'] = json_encode($permission_to_json);
        $UpdateGroupPermission['update_date'] = Carbon::now();
        $UpdateGroupPermission['update_by'] =\Auth::user()->user;


        // dd($UpdateGroupPermission);

        $GroupPermission->where('id', $request->input('id'))
            ->update($UpdateGroupPermission);
        flashMe()->success();
        return redirect('manage/permission');

    }

    public function create()
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $GroupUser = new GroupUser;
        $GroupUser = $GroupUser->where('group_user_id', '!=', 1)->get();

        $ModuleID = GroupPermission::select('module_id')->groupBy('module_id')->get();

        $GroupUserID = GroupUser::select('group_user_id')->groupBy('group_user_id')->get();

        $idModule = [];

        foreach ($ModuleID as $key => $value) {
            array_push($idModule, $value->module_id);
        }

        $idGroupUser = [];

        foreach ($idGroupUser as $key => $value) {
            array_push($idGroupUser, $value->group_user_id);
        }

        $Module = new Module;
        $Module = $Module->get();

        return view('manage.groupPermission.formgroupPermission', ['GroupUser' => $GroupUser, 'Module' => $Module]);
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $GroupPermission = new GroupPermission();

        $edit = $GroupPermission->where('id', $id)->get();

        //  dd($edit);

        $GroupUser = new GroupUser;
        $GroupUser = $GroupUser->where('group_user_id', '!=', 1)->get();

//  dd($edit);

        $accessData = json_decode($edit[0]->access_data);

        $Module = new Module;
        $Module = $Module->get();

        return view('manage.groupPermission.formgroupPermission', ['edit' => $edit, 'GroupUser' => $GroupUser, 'Module' => $Module, 'accessData' => $accessData]);

    }

    public function destroy(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }
        //  dd($id);

        $GroupPermission = new GroupPermission();
        $GroupPermission->where('id', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/permission');
    }

    public function getModule(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');

            //   $dataTable = $request->get('dataTable');

            $GroupPermission = new GroupPermission();
            $GroupPermission = $GroupPermission->where('group_user_id', $query)->get();

            $moduleID = [];

            foreach ($GroupPermission as $key => $value) {
                array_push($moduleID, $value->module_id);
            }

            $Module = new Module;
            $data = $Module->WhereNotIn('module_id', $moduleID)->get();

            $output = '';
            foreach ($data as $row) {
                $output .= '
         <option value="' . $row->module_id . '"' . 'name="' . $row->module_name . '"' . '>' . $row->module_title . '</option>';
            }
            // $output .= '</ul>';
            echo $output;
        }

    }
}
