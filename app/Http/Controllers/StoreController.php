<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\Module;
use App\Models\Store;
use App\Models\ProductTemplate;
use App\Models\TemplateRedeem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Validator;

class StoreController extends Controller
{
    //

    public $module = 'manage/store';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);

            return $next($request);
        });

    }

    public function index(Request $request)
    {
        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $store = new Store();

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) == "1") {
            if ($request->session()->has('bu')) {
                if (!empty($input['q'])) {
                    if ($request->session()->get('bu') == 'all') {
                        $store = $store->where('store_name', 'like', '%' . trim($input['q']) . '%')->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
                    } else {
                        $store = $store->where('store_name', 'like', '%' . trim($input['q']) . '%')->where('bu_id', $request->session()->get('bu'))->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
                    }

                } else {
                    if ($request->session()->get('bu') == 'all') {
                        $store = $store->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);

                    } else {
                        $store = $store->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->where('bu_id', $request->session()->get('bu'))->orderBy('create_date','desc')->paginate(10);
                    }

                }
            } else {
                if (!empty($input['q'])) {
                    $store = $store->where('store_name', 'like', '%' . trim($input['q']) . '%')->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
                } else {
                    $store = $store->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
                }
            }

        } else if (trim(\Auth::user()->group_user_id) == "2") {
            if (!empty($input['q'])) {
                $store = $store->where('store_name', 'like', '%' . trim($input['q']) . '%')->where('bu_id', trim(\Auth::user()->bu_id))->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);;
            } else {
                $store = $store->where('bu_id', trim(\Auth::user()->bu_id))->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
            }
        } else {

            $store = new Store;
            // $store = $store->where('store_id', \Auth::user()->store_id)->with('BusinessUnit')->first();

            if (!empty($input['q'])) {
                $store = $store->where('store_id', \Auth::user()->store_id)->where('store_name', 'like', '%' . trim($input['q']) . '%')->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
            } else {
                // if(isset($store->bu_id))
                $store = $store->where('store_id', \Auth::user()->store_id)->with(['BusinessUnit', 'ProductTemplate', 'TemplateRedeem'])->orderBy('create_date','desc')->paginate(10);
            }
        }

        // foreach($store as $key=>$data){
        //     print_r($data->ProductTemplate['']);
        // }

        return view('manage.store.store', ['store' => $store]);
    }

    public function create()
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;

        if (trim(\Auth::user()->group_user_id) == "3") {

            $store = new Store;
            $store = $store->where('store_id', \Auth::user()->store_id)->with('BusinessUnit')->first();

            $businessUnit = $businessUnit->where('bu_id', $store->bu_id)->get();

            // dd($store[0]->bu_id);
        } else if (trim(\Auth::user()->group_user_id) == "2") {

            $businessUnit = $businessUnit->where('bu_id', \Auth::user()->bu_id)->get();

            // dd($store[0]->bu_id);
        } else {
            $businessUnit = $businessUnit->get();
        }

        $templateProduct = new ProductTemplate;
        $templateProduct = $templateProduct->get();

        $templateRedeem = new TemplateRedeem;
        $templateRedeem = $templateRedeem->get();

        return view('manage.store.formstore', compact(['businessUnit', 'templateProduct', 'templateRedeem']));
    }

    public function update(Request $request)
    {
        $store = new Store();

        $validator = Validator::make($request->all(), [
            'store_name' => 'required|max:255',
            'tid' => 'required',
            'mid' => 'required',
            'template_product' => 'required',
            'template_redeem' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        } else {

            $update['store_name'] = $request->input('store_name');
            $update['tid'] = $request->input('tid');
            $update['mid'] = $request->input('mid');
            $update['tmppd_id'] = $request->input('template_product');
            $update['tmprd'] = $request->input('template_redeem');
            $update['update_by'] = \Auth::user()->user;
            $update['update_date'] = Carbon::now();
            $update['address'] = $request->input('address');
            $update['tel'] = $request->input('tel');
            $update['email'] = $request->input('email');

            $store->where('store_id', $request->input('id'))
                ->update($update);
            flashMe()->success();
            return redirect('manage/store');
        }
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $store = new Store();

        $edit = $store->where('store_id', $id)->get();

        $businessUnit = new BusinessUnit;
        $businessUnit = $businessUnit->get();

        $templateProduct = new ProductTemplate;
        $templateProduct = $templateProduct->get();

        $templateRedeem = new TemplateRedeem;
        $templateRedeem = $templateRedeem->get();

        return view('manage.store.formstore', compact(['edit', 'businessUnit', 'templateProduct', 'templateRedeem']));
    }

    public function store(Request $request)
    {

        $store = new Store;

        $validator = Validator::make($request->all(), [
            'store_name' => 'required|max:255',
            'tid' => 'required',
            'mid' => 'required',
            'businessunit' => 'required',
            'template_product' => 'required',
            'template_redeem' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('manage/store/getform')
                ->withErrors($validator)
                ->withInput();
        } else {

            $store->store_name = $request->input('store_name');
            $store->store_id = ($store->max('store_id') + 1) . '';
            $store->tid = $request->input('tid');
            $store->mid = $request->input('mid');
            $store->bu_id = $request->input('businessunit');
            $store->tmppd_id = $request->input('template_product');
            $store->tmprd = $request->input('template_redeem');
            $store->address = $request->input('address');
            $store->tel = $request->input('tel');
            $store->email = $request->input('email');
            $store->create_by = \Auth::user()->user;
            $store->create_date = Carbon::now();

//  dd($request->all());
            $store->save();
            flashMe()->success();
            return redirect('manage/store');

        }

    }

    public function destroy(Request $request)
    {
        //  dd($id);

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $store = new Store();
        $store->where('store_id', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/store');
    }

    public function getTemplateRedeem(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');

            //   $dataTable = $request->get('dataTable');


            $TemplateRedeem = new TemplateRedeem;
            $data = $TemplateRedeem
                ->where('bu_id', $query)
                ->get();
            $output = '';
            foreach ($data as $row) {
                $output .= '
       <option value="' . $row->tmprd_id . '"' . 'name="' . $row->tmprd_name . '"' . '>' . $row->tmprd_name . '</option>
       ';
            }
            // $output .= '</ul>';

            echo $output;
        }

    }

    public function getTemplateProduct(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');

            //   $dataTable = $request->get('dataTable');

            $ProductTemplate = new ProductTemplate;
            $data = $ProductTemplate
                ->where('bu_id', $query)
                ->get();
            $output = '';
            foreach ($data as $row) {
                $output .= '
       <option value="' . $row->tmppd_id . '"' . 'name="' . $row->tmppd_name . '"' . '>' . $row->tmppd_name . '</option>
       ';
            }
            // $output .= '</ul>';
            echo $output;
        }

    }
}
