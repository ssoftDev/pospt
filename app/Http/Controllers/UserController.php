<?php

namespace App\Http\Controllers;

use App\Mail\mailUser;
use App\Mail\ResetPasswordMail;
use App\Models\BusinessUnit;
use App\Models\GroupUser;
use App\Models\Module;
use App\Models\Store;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Input;
use Mail;

class UserController extends Controller
{

    public $module = 'manage/users';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }
    //
    public function index(Request $request)
    {
        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $user = new User();

        $input = $request->all();

        if (!empty($input['q'])) {
            if ($input['s'] == 'S') {
                $store = new Store;
                $data = $store
                   ->where('store_name', 'like', '%' . trim($input['q']) . '%')
                    ->first();

                $user_data = $user->where('store_id',$data->store_id)->orderBy('create_date', 'desc')->with('Store')->paginate(10);

            } else if ($input['s'] == 'U') {
                $user_data = $user->where('user', 'like', '%' . trim($input['q']) . '%')->orderBy('create_date', 'desc')->with('Store')->paginate(10);

            }

        } else {
            $user_data = $user->with('Store')->orderBy('create_date', 'desc')->paginate(10);
        }

        // foreach($user_data as $key=>$data){
        //     echo $data->Store->store_name;
        // }

        //   print_r($user_data);

        return view('manage.user.user', ['user' => $user_data]);
    }

    public function create()
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $GroupUser = new GroupUser;
        $GroupUser = $GroupUser->get();

        $user = 'LUM' . $this->random('NO', 2) . $this->random('LLS', 3);
        $password = $this->random('LL', 1) . $this->random('LS', 1) . $this->random('NO', 1) . $this->random('ALL', 5);

        $businessUnit = new BusinessUnit;

        $businessUnit = $businessUnit->get();

        return view('manage.user.formuser', ['user' => $user, 'password' => $password, 'GroupUser' => $GroupUser, 'businessUnit' => $businessUnit]);
    }

    public function store(Request $request)
    {

        // dd($request->input('group_user'));

        $checkUserId = User::where('user',$request->input('user'))->count();


        if($checkUserId!=0){
            return redirect()
    ->back()
    ->withErrors(['User ซ้ำ'])
    ->withInput();

        }

        $user = new User();

        $store = new Store;
        $data = $store
            ->where('store_id', $request->input('store'))
            ->first();

        //    dd($request->all());

        if ($request->input('group_user') == 2) {
            $user->bu_id = $request->input('businessunit');
        } else if ($request->input('group_user') == 3) {
            $user->store_id = $request->input('store');
            $user->bu_id = $data['bu_id'];
        }

        $user->user = $request->input('user');
        // $user->id = ($user->max('id') + 1) . '';
        $user->email = $request->input('email');
        $user->tel = $request->input('tel');
        $user->pass = md5($request->input('pass'));
        $user->create_by = \Auth::user()->user;
        $user->create_date = Carbon::now();

        $user->send = $request->input('send');
        $user->group_user_id = $request->input('group_user');

        $user->save();

        // dd($request->input('email'));

        $store = new Store;
        $store_name = $store
            ->select('store_name')
            ->where('store_id', $user->store_id)
            ->first();

        $GroupUser = new GroupUser;
        $GroupUser = $GroupUser->where('group_user_id', $request->input('group_user'))->get();

        // dd($GroupUser);

        $detail = [];

        if ($request->input('group_user') == 2) {
            $detail['bu'] = $request->input('businessunit');

            $businessUnit = new BusinessUnit;

            $businessUnit = $businessUnit->where('bu_id', $request->input('businessunit'))->first();

            $detail['bu'] = $businessUnit->bu_name;

            $detail['storeid'] = null;

        } else if ($request->input('group_user') == 3) {
            $detail['storeid'] = $data['store_id'];
            $detail['store'] = $store_name->store_name;

            $businessUnit = new BusinessUnit;

            $businessUnit = $businessUnit->where('bu_id', $data['bu_id'])->first();

            $detail['bu'] = $businessUnit->bu_name;
        }

        $detail['user'] = $request->input('user');
        $detail['pass'] = $request->input('pass');
        $detail['email'] = $request->input('email');
        $detail['tel'] = $request->input('tel');
        $detail['group'] = $GroupUser[0]->group_user_name;

        $detail['createby'] = \Auth::user()->user;
        $detail['title'] = "Post PT User : " . $request->input('user');
        // $detail['storeid'] = $data['store_id'];

        if ($request->input('send') == "EMAIL") {
            Mail::to($request->input('email'))->send(new mailUser($detail));
        }

        flashMe()->success();

        return redirect('manage/users');
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $user = new User();

        $GroupUser = new GroupUser;
        $GroupUser = $GroupUser->get();

        $edit = $user->where('user', $id)->with('Store')->get();

        $businessUnit = new BusinessUnit;

        $businessUnit = $businessUnit->get();

        return view('manage.user.formuser', ['edit' => $edit, 'GroupUser' => $GroupUser, 'businessUnit' => $businessUnit]);
    }

    public function destroy(Request $request)
    {
        //  dd($id);

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $user = new User();
        $user->where('user', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/users');
    }

    public function update(Request $request)
    {
        $user = new User();

        $checkUserId = User::where('user', $request->input('user'))->count();

if ($checkUserId != 0) {
    return redirect()
        ->back()
        ->withErrors(['User ซ้ำ'])
        ->withInput();

}


        $store = new Store;
        $data = $store
            ->where('store_id', $request->input('store'))
            ->first();

        //   dd($request->input('store'));

        if ($request->input('group_user') == 2) {
            $update['bu_id'] = $request->input('businessunit');
        } else if ($request->input('group_user') == 3) {
            $update['store_id'] = $request->input('store');
            $update['bu_id'] = $data['bu_id'];
        }

        $update['email'] = $request->input('email');
        // $update['user'] = $request->input('user');
        $update['tel'] = $request->input('tel');
        $update['group_user_id'] = $request->input('group_user');
        $update['update_date'] = Carbon::now();

        // $update['store_id'] = $data->store_id;
        // $detail['group'] = $request->input('group_user');
        $update['update_by'] = \Auth::user()->user;

        // dd($request->input('id'));
        $user->where('user', $request->input('id'))
            ->update($update);
        flashMe()->success();
        return redirect('manage/users');
    }

    public function ResetPassword(Request $request)
    {
        $user = new User();

        $data = $user->where('user', $request->input('idreset'))->first();

        // dd($data->user);

        $pass = $this->random('LL', 1) . $this->random('LS', 1) . $this->random('NO', 1) . $this->random('ALL', 5);

        $update['pass'] = md5($pass);

        $detail['user'] = $data->user;
        $detail['pass'] = $pass;
        $detail['email'] = $data->email;
        $detail['tel'] = $data->tel;
        // $detail['group'] = $data->group_user;
        // $detail['store'] = $data->store->store_name;
        $detail['createby'] = \Auth::user()->user;
        $detail['title'] = "Reset Password PT User : " . $data->user;

        Mail::to($data->email)->send(new ResetPasswordMail($detail));

        $user->where('user', $request->input('idreset'))
            ->update($update);
        flashMe()->success();

        return redirect('manage/users');
    }

    public function getStore(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');
            $store = new Store;
            $data = $store
                ->where('store_name', 'LIKE', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row) {
                $output .= '
       <li><a href="#" style="color: #0400ff;
">' . $row->store_name . '</a></li>
       ';
            }
            $output .= '</ul>';
            echo $output;
        }

    }

    public function random($type, $length)
    {
        $pool;
        switch ($type) {
            case 'LS':
                $pool = 'abcdefghijklmnopqrstuvwxyz';
                break;
            case 'LL':
                $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'LLS':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'NO':
                $pool = '0123456789';
                break;
            case 'ALL':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
        }

        return substr(str_shuffle(str_repeat($pool, 10)), 0, $length);
    }
}
