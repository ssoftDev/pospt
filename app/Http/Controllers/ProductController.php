<?php

namespace App\Http\Controllers;

use App\Models\GroupProduct;
use App\Models\Module;
use App\Models\MP_Product;
use App\Models\Product;
use App\Models\TypeProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //

    public $module = 'manage/product';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $groupProduct = new GroupProduct;

        if (trim(\Auth::user()->group_user_id) != "1") {

            // $store = new Store;
            // $store = $store->where('store_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->first();

            $groupProduct = $groupProduct->where('bu_id', trim(\Auth::user()->bu_id))->get();

            // dd($store[0]->bu_id);
        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                $groupProduct = $groupProduct->where('bu_id', $request->session()->get('bu'))->get();

            } else {

                $groupProduct = $groupProduct->get();
            }
        }

        $product = null;

        // $product = new Product();

        $input = $request->all();
        // dd($input);
        if (isset($input['gp']) && isset($input['type'])) {
      


            if ($input['gp'] == 'all' && $input['type'] != 'all') {
                $product = Product::join('type_product', 'product.type_product_id', '=', 'type_product.type_product_id')
                    ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                    ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                    ->where('product.type_product_id', $input['type'])
                    ->where('product.p_name', 'like' , '%'.$input['q'].'%')
                    ->orderBy('product.create_date', 'desc')->paginate(10);

            }else if($input['gp'] != 'all' && $input['type'] == 'all'){
                      $product = Product::join('type_product', 'product.type_product_id', '=', 'type_product.type_product_id')
                    ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                    ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                    ->where('group_product.gp_id', $input['gp'])
                    ->where('product.p_name', 'like' , '%'.$input['q'].'%')
                    ->orderBy('product.create_date', 'desc')->paginate(10);
            }
             else if($input['gp'] != 'all' && $input['type'] != 'all'){
                      $product = Product::join('type_product', 'product.type_product_id', '=', 'type_product.type_product_id')
                    ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                    ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                    ->where('group_product.gp_id', $input['gp'])
                    ->where('product.type_product_id', $input['type'])
                    ->where('product.p_name', 'like' , '%'.$input['q'].'%')
                    ->orderBy('product.create_date', 'desc')->paginate(10);
            }else{
                      $product = Product::join('type_product', 'product.type_product_id', '=', 'type_product.type_product_id')
                    ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                    ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                    ->where('product.p_name', 'like' , '%'.$input['q'].'%')
                    ->orderBy('product.create_date', 'desc')->paginate(10);
            }

        } else {
            
            $product = Product::
                join('type_product', 'product.type_product_id', '=', 'type_product.type_product_id')
                ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                ->orderBy('product.create_date', 'desc')->paginate(10);
        }

        // foreach($store as $key=>$data){
        //     print_r($data->TemplateProduct['']);
        // }

        return view('manage.product.product', ['product' => $product, 'groupProduct' => $groupProduct]);
    }

    public function create(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $groupProduct = new GroupProduct();

        if (trim(\Auth::user()->group_user_id) != "1") {

            // $store = new Store;
            // $store = $store->where('store_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->first();

            $groupProduct = $groupProduct->where('bu_id', trim(\Auth::user()->bu_id))->where('is_enable', 'y')->get();

            // dd($store[0]->bu_id);
        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                $groupProduct = $groupProduct->where('is_enable', 'y')->where('bu_id', $request->session()->get('bu'))->get();

            } else {

                $groupProduct = $groupProduct->where('is_enable', 'y')->get();
            }

        }

        $typeProduct = new TypeProduct();
        $typeProduct = $typeProduct->get();

        return view('manage.product.formproduct', compact('groupProduct', 'typeProduct'));
    }

    public function store(Request $request)
    {

        $product = new Product;

        $product->p_name = $request->input('p_name');
        $product->pid = $this->random('LL', 5) . $this->random('NO', 2);
        $product->p_price = $request->input('price');
        $product->type_product_id = $request->input('p_type');
        $product->create_date = Carbon::now();
        $product->create_by = \Auth::user()->user;

        $mpProduct = new MP_Product();
        $mpProduct->gp_id = $request->input('p_group');
        $mpProduct->pid = $product->pid;

        if ($request->hasFile('productpic')) {
            // image upload
            $image = $request->file('productpic')->getClientOriginalName();
            $destination = base_path() . '/public/files/product/image';
            $imagename = uniqid() . '_' . $image;
            $request->file('productpic')->move($destination, $imagename);

            $product->img_url = asset('files/product/image') . '/' . $imagename;
        }

        $product->save();
        $mpProduct->save();
        flashMe()->success();
        return redirect('manage/product');
    }

    public function edit(Request $request,$id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $product = new Product();

        $edit = $product->where('pid', $id)->get();

        //  dd($edit);

        $groupProduct = new GroupProduct();

        if (trim(\Auth::user()->group_user_id) != "1") {

            // $store = new Store;
            // $store = $store->where('store_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->first();

            $groupProduct = $groupProduct->where('bu_id', trim(\Auth::user()->bu_id))->where('is_enable', 'y')->get();

            // dd($store[0]->bu_id);
        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                $groupProduct = $groupProduct->where('is_enable', 'y')->where('bu_id', $request->session()->get('bu'))->get();

            } else {

                $groupProduct = $groupProduct->where('is_enable', 'y')->get();
            }

        }

        $typeProduct = new TypeProduct();
        $typeProduct = $typeProduct->get();

        $mpProduct = new MP_Product();
        $mpProduct = $mpProduct->where('pid', $id)->first();

        return view('manage.product.formproduct', compact(['edit', 'groupProduct', 'typeProduct', 'mpProduct']));
    }

    public function update(Request $request)
    {
        $product = new Product();

        $updateproduct['p_name'] = $request->input('p_name');
        $updateproduct['p_price'] = $request->input('price');
        $updateproduct['type_product_id'] = $request->input('p_type');
        $updateproduct['update_by'] = \Auth::user()->user;
        $updateproduct['update_date'] = Carbon::now();

        $mpProduct = new MP_Product();
        $mpProduct->gp_id = $request->input('p_group');
        $mpProduct->pid = $product->pid;

        $updateproductTab['gp_id'] = $request->input('p_group');

        if ($request->hasFile('productpic')) {
            // image upload
            $image = $request->file('productpic')->getClientOriginalName();
            $destination = base_path() . '/public/files/product/image';
            $imagename = uniqid() . '_' . $image;
            $request->file('productpic')->move($destination, $imagename);

            $updateproduct['img_url'] = asset('files/product/image') . '/' . $imagename;
        }

        $product->where('pid', $request->input('id'))
            ->update($updateproduct);

        $mpProduct->where('pid', $request->input('id'))
            ->update($updateproductTab);
        flashMe()->success();
        return redirect('manage/product');
    }

    public function random($type, $length)
    {
        $pool;
        switch ($type) {
            case 'LS':
                $pool = 'abcdefghijklmnopqrstuvwxyz';
                break;
            case 'LL':
                $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'LLS':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'NO':
                $pool = '0123456789';
                break;
            case 'ALL':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
        }

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function destroy(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        //  dd($id);

        $product = new Product();
        $product->where('pid', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/product');
    }

}
