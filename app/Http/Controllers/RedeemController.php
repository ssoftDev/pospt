<?php

namespace App\Http\Controllers;

use App\Models\BusinessUnit;
use App\Models\Module;
use App\Models\Redeem;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RedeemController extends Controller
{
    //

    public $module = 'manage/redeem';

    public function __construct()
    {
        // if (!isset($this->data)) {
        //     $this->data = new \stdClass();
        // }

        // $this->beforeFilter('csrf', array('on' => 'post'));
        // $this->model = new Popup();

        $this->middleware(function ($request, $next) {
            $this->info = Module::makeInfo($this->module);
            $this->access = Module::validAccess($this->info['id']);
            // dd($this->info);

            return $next($request);
        });

    }

    public function index(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_view'] == 0 || !$this->access['is_view']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $Redeem = new Redeem;

        $input = $request->all();

        if (trim(\Auth::user()->group_user_id) != "1") {

            if (!empty($input['q'])) {
                $Redeem = $Redeem->where('bu_id', trim(\Auth::user()->bu_id))->where('rd_code', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
            } else {
                $Redeem = $Redeem->where('bu_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
            }

        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                if (!empty($input['q'])) {
                    $Redeem = $Redeem->where('rd_code', 'like', '%' . trim($input['q']) . '%')->where('bu_id',$request->session()->get('bu'))->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $Redeem = $Redeem->with('BusinessUnit')->where('bu_id',$request->session()->get('bu'))->orderBy('create_date', 'desc')->paginate(10);
                }

            } else {
                if (!empty($input['q'])) {
                    $Redeem = $Redeem->where('rd_code', 'like', '%' . trim($input['q']) . '%')->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                } else {
                    $Redeem = $Redeem->with('BusinessUnit')->orderBy('create_date', 'desc')->paginate(10);
                }

            }

        }

        return view('manage.redeem.redeem', compact('Redeem'));
    }

    public function create(Request $request)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_add'] == 0 || !$this->access['is_add']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $businessUnit = new BusinessUnit;

        if (trim(\Auth::user()->group_user_id) != "1") {

            // $store = new Store;
            // $store = $store->where('store_id', trim(\Auth::user()->bu_id))->with('BusinessUnit')->first();

            $businessUnit = $businessUnit->where('bu_id', trim(\Auth::user()->bu_id))->get();

            // dd($store[0]->bu_id);
        } else {

            if ($request->session()->has('bu') && $request->session()->get('bu') != 'all') {
                $businessUnit = $businessUnit->where('bu_id', $request->session()->get('bu'))->get();

            } else {

                $businessUnit = $businessUnit->get();
            }
        }

        return view('manage.redeem.formredeem', compact('businessUnit'));
    }

    public function update(Request $request)
    {
        $Redeem = new Redeem();

        $updateRedeem['rd_code'] = $request->input('rd_code');
        $updateRedeem['rd_desc'] = $request->input('rd_desc');
        $updateRedeem['rd_point'] = $request->input('rd_point');
        $updateRedeem['is_redeem'] = $request->input('status');
        $updateRedeem['is_discount'] = $request->input('discount');
        $updateRedeem['update_by'] = \Auth::user()->user;
        $updateRedeem['update_date'] = Carbon::now();

        $updateRedeem['discount_price'] = $request->input('discount_price');
        // $updateRedeem['bu_id'] = $request->input('businessunit');

        if ($request->hasFile('redeempic')) {
            // image upload
            $image = $request->file('redeempic')->getClientOriginalName();
            $destination = base_path() . '/public/files/redeem/image';
            $imagename = uniqid() . '_' . $image;
            $request->file('redeempic')->move($destination, $imagename);

            // $updateRedeem['img_url'] = $imagename;

            $updateRedeem['img_url'] = asset('files/redeem/image') . '/' . $imagename;

        }

        $Redeem->where('rd_id', $request->input('id'))
            ->update($updateRedeem);

        flashMe()->success();

        return redirect('manage/redeem');
    }

    public function edit($id)
    {

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_edit'] == 0 || !$this->access['is_edit']) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $Redeem = new Redeem;

        $edit = $Redeem->where('rd_id', $id)->get();

        $businessUnit = new BusinessUnit;
        $businessUnit = $businessUnit->get();

        return view('manage.redeem.formredeem', compact(['edit', 'businessUnit']));
    }

    public function store(Request $request)
    {
        $Redeem = new Redeem;

        $Redeem->rd_code = $request->input('rd_code');
        $Redeem->rd_id = ($Redeem->max('rd_id') + 1) . '';
        $Redeem->rd_desc = $request->input('rd_desc');
        $Redeem->rd_point = $request->input('rd_point');
        $Redeem->is_redeem = $request->input('status');
        $Redeem->is_discount = $request->input('discount');
        $Redeem->discount_price = $request->input('discount_price');
        $Redeem->bu_id = $request->input('businessunit');
        $Redeem->create_by = \Auth::user()->user;
        $Redeem->create_date = Carbon::now();

        if ($request->hasFile('redeempic')) {
            // image upload
            $image = $request->file('redeempic')->getClientOriginalName();
            $destination = base_path() . '/public/files/redeem/image';
            $imagename = uniqid() . '_' . $image;
            $request->file('redeempic')->move($destination, $imagename);

            $Redeem->img_url = asset('files/redeem/image') . '/' . $imagename;
        }

        $Redeem->save();
        flashMe()->success();

        return redirect('manage/redeem');
    }

    public function destroy(Request $request)
    {
        //  dd($id);

        if (trim(\Auth::user()->group_user_id) != "1") {
            if ($this->access['is_remove'] == 0) {
                flashMe()->error();
                return \Redirect::to('/');
            }
        }

        $Redeem = new Redeem();
        $Redeem->where('rd_id', $request->input('iddelete'))->delete();
        flashMe()->success();
        return redirect('manage/redeem');
    }

}
