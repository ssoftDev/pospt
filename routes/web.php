<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');

Route::get('/mail',function(){
    return view('mail.mailuser');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'manage', 'middleware' => 'auth'],function () {
    
    Route::get('users', 'UserController@index');

    Route::post('users', 'UserController@store');

    Route::post('users/delete','UserController@destroy');

    Route::get('users/edit/{id}', 'UserController@edit');

    Route::put('users', 'UserController@update');

  //  Route::post

    Route::get('users/getform', 'UserController@create');

    Route::post('users/getstore', 'UserController@getStore');

    Route::post('users/resetpassword', 'UserController@ResetPassword');



    Route::get('business-unit', 'BusinessUnitController@index');

    Route::get('business-unit/edit/{id}', 'BusinessUnitController@edit');

    Route::put('business-unit', 'BusinessUnitController@update');

    Route::post('business-unit/delete','BusinessUnitController@destroy');

    Route::post('business-unit', 'BusinessUnitController@store');

    Route::post('business-unit/setbu', 'BusinessUnitController@SetBu_Session');


    



    Route::get('store','StoreController@index');

    Route::post('store','StoreController@store');

    Route::get('store/edit/{id}', 'StoreController@edit');

    Route::put('store', 'StoreController@update');

    Route::post('store/delete','StoreController@destroy');

    Route::get('store/getform', 'StoreController@create');

    Route::post('store/getTemplateProduct', 'StoreController@getTemplateProduct');

    Route::post('store/getTemplateRedeem', 'StoreController@getTemplateRedeem');




    Route::get('product','ProductController@index');

    Route::post('product','ProductController@store');

    Route::get('product/getform', 'ProductController@create');

    Route::get('product/edit/{id}', 'ProductController@edit');

    Route::put('product', 'ProductController@update');

    Route::post('product/delete','ProductController@destroy');



    Route::get('group-product','GroupProductController@index');

    Route::post('group-product','GroupProductController@store');

    Route::get('group-product/getform', 'GroupProductController@create');

    Route::get('group-product/edit/{id}', 'GroupProductController@edit');

    Route::put('group-product', 'GroupProductController@update');

    Route::post('group-product/delete','GroupProductController@destroy');


    Route::get('group-template-product','GroupTemplateProductController@index');

    Route::post('group-template-product','GroupTemplateProductController@store');

    Route::get('group-template-product/getform', 'GroupTemplateProductController@create');

    Route::post('group-template-product/getGroupProduct', 'GroupTemplateProductController@getGroupProduct');

    Route::post('group-template-product/getProduct', 'GroupTemplateProductController@getProduct');

    Route::get('group-template-product/edit/{id}', 'GroupTemplateProductController@edit');

    Route::put('group-template-product', 'GroupTemplateProductController@update');

    Route::post('group-template-product/delete','GroupTemplateProductController@destroy');


    Route::get('redeem','RedeemController@index');

    Route::post('redeem','RedeemController@store');

    Route::get('redeem/getform', 'RedeemController@create');

    Route::get('redeem/edit/{id}', 'RedeemController@edit');

    Route::put('redeem', 'RedeemController@update');

    Route::post('redeem/delete','RedeemController@destroy');

    
    Route::get('redeem-template','RedeemTemplateController@index');

    Route::post('redeem-template','RedeemTemplateController@store');

    Route::get('redeem-template/getform', 'RedeemTemplateController@create');

    Route::get('redeem-template/edit/{id}', 'RedeemTemplateController@edit');

    Route::put('redeem-template', 'RedeemTemplateController@update');

    Route::post('redeem-template/delete','RedeemTemplateController@destroy');

    Route::post('redeem-template/getRedeem', 'RedeemTemplateController@getRedeem');

    
    Route::get('setup-earn','EarnController@index');

    Route::post('setup-earn','EarnController@store');

    Route::get('setup-earn/getform', 'EarnController@create');

    Route::post('setup-earn/getProduct', 'EarnController@getProduct');

    Route::get('setup-earn/edit/{id}', 'EarnController@edit');

    Route::put('setup-earn', 'EarnController@update');

    Route::post('setup-earn/delete','EarnController@destroy');

    Route::post('setup-earn/getEarn', 'EarnController@getEarn');

    

     Route::get('permission','GroupPermissionController@index');

    Route::post('permission','GroupPermissionController@store');

    Route::get('permission/getform', 'GroupPermissionController@create');

    Route::get('permission/edit/{id}', 'GroupPermissionController@edit');

    Route::put('permission', 'GroupPermissionController@update');

    Route::post('permission/delete','GroupPermissionController@destroy');

    Route::post('permission/getModule','GroupPermissionController@getModule');

    Route::get('report','ReportController@index');
    Route::post('report','ReportController@Report');
    Route::get('report/{type}/{report}/{store}/{start_end}/{maxcard?}','ReportController@displayReport');

    // Route::post('setup-earn/getRedeem', 'RedeemTemplateController@getRedeem');




});