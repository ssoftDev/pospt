<?php 

return [
	'success' => [
		'type' => 'success',
		'title' => 'Success',
		'message' => 'บันทึกข้อมูลเสร็จสิ้น!',
		'options' => [
			'position' => 'topRight', // this is an example option, you can add another option
			'transitionIn' => 'bounceInLeft',
    			'transitionOut' => 'fadeOut',
		],
	],
	'info' => [
		'type' => 'info',
		'title' => 'Hi...',
		'message' => 'FlashMe is Ready!',
		'options' => [
			'position' => 'topRight', // this is an example option, you can add another option
			'transitionIn' => 'bounceInLeft',
    			'transitionOut' => 'fadeOut',
		],
	],
	'warning' => [
		'type' => 'warning',
		'title' => 'Hi...',
		'message' => 'FlashMe is Ready!',
		'options' => [
			'position' => 'topRight', // this is an example option, you can add another option
			'transitionIn' => 'bounceInLeft',
    			'transitionOut' => 'fadeOut',
		],
	],
	'error' => [
		'type' => 'error',
		'title' => 'Permission',
		'message' => 'ไม่มีสิทธิ์เข้าถึง',
		'options' => [
			'position' => 'topRight', // this is an example option, you can add another option
			'transitionIn' => 'bounceInLeft',
    			'transitionOut' => 'fadeOut',
		],
	],
];