@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Report<small>ระบบ Report</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Report</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/report/') }}" method="POST" autocomplete="off">
                        {{ csrf_field() }}
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->store_id  }}">
                        @endif
                            <div class="box-body">

                                    <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Type</label>
                          
                                            <div class="col-sm-10">
                                                <div class="input-group" style="width:100%">
                                                        <select class="form-control" id="report_type" name="report_type" @if(isset($edit) ) disabled @endif>

                                                          <option value="earn" selected @if($input['report_type']=="earn") selected @endif >Earn</option>
                                                                    <option value="redeem" @if($input['report_type']=="redeem") selected @endif>Redeem</option>
                                                                    
                                                                    
                                                        </select>
                                                          <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                                  </div>
                                            </div>
                                           
                                          </div>
                              
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Report</label>
                  
                                    <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                                <select class="form-control" id="report" name="report" @if(isset($edit) ) disabled @endif>
                                                            {{-- <option value="tredeem" @if($input['report']=="tredeem") selected @endif>Transection Redeem</option>
                                                            <option value="sredeem" @if($input['report']=="sredeem") selected @endif>Summary Redeem</option> --}}
                                                </select>
                                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                          </div>
                                    </div>
                                   
                                  </div>

                                  <div class="form-group" id="maxcard" hidden="true">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Maxcard No.</label>
                      
                                        <div class="col-sm-10">
                                            <div class="input-group" style="width:100%">
                                                
                                                            <input type="text" id="maxcard_no" value="{{ $input['maxcard_no'] }}" name="maxcard_no" class="form-control">
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                    
                                              </div>
                                        </div>
                                       
                                      </div>

                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Store</label>
                  
                                    <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                                <select class="form-control" id="store" name="store" @if(isset($edit) ) disabled @endif>
                                                          @if(trim(\Auth::user()->group_user_id) != "3")  <option  value="all">All</option>   @endif
                                                        {{-- {{ dd(old('store')) }} --}}
                                                    @foreach($Store as $key=>$value)
                                                            <option @if($input['store']==$value->store_id) selected  @endif value="{{ $value->store_id }}">{{ $value->store_name }}</option>
                                                        @endforeach
                                                        
                                                          </select>
                                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                          </div>
                                    </div>
                                   
                                  </div>

                                  <div class="form-group">

                                    <label for="inputEmail3" class="col-sm-2 control-label pull-left">Start Date - End Date</label>
                    
                                    <div class="col-sm-10">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                      </div>
                                      <input type="text" name="start_end" id="start_end" class="form-control pull-right" value="{{ $input['start_end'] }}"/>
                                    </div>
                                    <!-- /.input group -->
                                  </div>
                                </div>

                                <div class="form-group">
           
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left"></label>
                                        <div class="col-sm-10">
                                        <button type="submit" id="savebutton" class="btn btn-success">Export</button>
                                    </div>
     
                                </div>

                        

                                <hr>

                                @if($TranSectionRedeem!=null)

                                <div class="row" style="margin-top: 30px">
                                        <table class="table">
                                                <tbody><tr>
                                                  <th>Date Time</th>
                                                  <th>Branch No</th>
                                                  <th>Branch Name</th>
                                                  <th>Maxcard No</th>
                                                  <th>Redeem Code</th>
                                                  <th>Redeem Desc</th>
                                                  <th>Point</th>
                                                </tr>
                                                <?php 
                                                $total = 0;
                                                ?>
                                                @foreach($TranSectionRedeem as $key=>$value)
                                                <tr>
                                                  <td>{{ $value->create_date }}</td>
                                                  <td>{{ $value['Store']->store_id }}</td>
                                                  <td>{{ $value['Store']->store_name }}</td>
                                                  <td>{{ $value->maxcard_id }}</td>
                                                  <td>{{ $value['Redeem']->rd_code }}</td>
                                                  <td>{{ $value['Redeem']->rd_desc }}</td>
                                                  <td>{{ $value['Redeem']->rd_point }}</td>
                                                  <?php
                                                    $total+= $value['Redeem']->rd_point ;
                                                  ?>
                                                </tr>
                                                @endforeach
                                              </tbody>
                                              <tfoot>
                                                    <tr>
                                                      <th style="text-align: right" id="total" colspan="6">Total :</th>
                                                      <td>{{ $total }}</td>
                                                    </tr>
                                                   </tfoot>
                                            </table>

                                </div>

                                <div class="row" style="margin-top: 10px;margin-bottom: 20px">

                                        <div class="col-sm-10">
                                        <a href={{ url('manage/report/pdf') .'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])) }} style="width:60px;font-weight: bold"  class="btn btn-success">Pdf</a>
                                        <a href={{ url('manage/report/excel').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])) }} style="width:60px;font-weight: bold" class="btn btn-danger">Excel</a>
                                        <a href={{ url('manage/report/csv').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end']))  }} style="width:60px;font-weight: bold" class="btn btn-info">Csv</a>
                                    </div>
     
                                </div>

            
                                      @endif    

                                      @if($SummaryRedeem!=null)

                                <div class="row" style="margin-top: 30px">
                                        <table class="table">
                                                <tbody><tr>
                                                    <th>No</th>
                                                  <th>Branch No</th>
                                                  <th>Branch Name</th>
                                                  <th>Total Point</th>
                                                </tr>
                                                <?php 
                                                $total = 0;
                                                ?>
                                                @foreach($SummaryRedeem as $key=>$value)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                  <td>{{ $value['Store']->store_id }}</td>
                                                  <td>{{ $value['Store']->store_name }}</td>
                                                  <td>{{ $value->total }}</td>
                                                  <?php
                                                    $total+= $value->total ;
                                                  ?>
                                                </tr>
                                                @endforeach
                                              </tbody>
                                              <tfoot>
                                                    <tr>
                                                      <th style="text-align: right" id="total" colspan="3">Total :</th>
                                                      <td>{{ $total }}</td>
                                                    </tr>
                                                   </tfoot>
                                            </table>

                                </div>

                                <div class="row" style="margin-top: 10px;margin-bottom: 20px">

                                        <div class="col-sm-10">
                                                <a href={{ url('manage/report/pdf') .'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])) }} style="width:60px;font-weight: bold"  class="btn btn-success">Pdf</a>
                                                <a href={{ url('manage/report/excel').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end']))  }} style="width:60px;font-weight: bold" class="btn btn-danger">Excel</a>
                                                <a href={{ url('manage/report/csv').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end']))  }} style="width:60px;font-weight: bold" class="btn btn-info">Csv</a>
                                    </div>
     
                                </div>

            
                                      @endif    


                                      @if($TransectionHeader!=null)

                                      <div class="row" style="margin-top: 30px">
                                              <table class="table">
                                                      <tbody><tr>
                                                        <th>Trans ID</th>
                                                        <th>Date Time</th>
                                                        <th>Branch No</th>
                                                        <th>Branch Name</th>
                                                        <th>Maxcard No.</th>
                                                        <th>Warranty No.</th>
                                                        <th>Car Model</th>
                                                        <th>Car License</th>
                                                        <th>Chassis No.</th>
                                                        <th>Film Price</th>
                                                        <th>Sum</th>
                                                        <th>Discount</th>
                                                        <th>Total</th>
                                                        <th>Point</th>
                                                      </tr>
                                                      <?php 
                                                      $total_sumprice = 0;
                                                      $total_point = 0;
                                                      ?>
                                                      @foreach($TransectionHeader as $key=>$value)
                                                      <tr>
                                                        <td>{{ $value->id_trans }}</td>
                                                        <td>{{ $value->create_date }}</td>
                                                        <td>{{ $value['Store']->store_id }}</td>
                                                        <td>{{ $value['Store']->store_name }}</td>
                                                        <td>{{ $value->maxcard_id }}</td>
                                                        <td>{{ $value->car_insurance_no }}</td>
                                                        <td>{{ $value->car_generation }}</td>
                                                        <td>{{ $value->car_license }}</td>
                                                        <td>{{ $value->car_chassis_no }}</td>
                                                        <td>{{ $value->total_film }}</td>
                                                        <td>{{ number_format($value->total_price,2) }}</td>
                                                        <td>{{ number_format($value->discount,2) }}</td>
                                                        <td>{{ number_format($value->sum_price,2) }}</td>
                                                        <td>{{ number_format($value->point,2) }}</td>
                                                        <?php
                                                          $total_sumprice+= $value->sum_price ;
                                                          $total_point+= $value->point ;
                                                        ?>
                                                      </tr>
                                                      @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                          <tr>
                                                            <th style="text-align: right" id="total" colspan="12">Total :</th>
                                                            <td>{{ number_format($total_sumprice,2) }}</td>
                                                            <td>{{ number_format($total_point,2) }}</td>
                                                          </tr>
                                                         </tfoot>
                                                  </table>
      
                                      </div>
      
                                      <div class="row" style="margin-top: 10px;margin-bottom: 20px">
      
                                              <div class="col-sm-10">
                                              <a href={{ url('manage/report/pdf') .'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no'] }} style="width:60px;font-weight: bold"  class="btn btn-success">Pdf</a>
                                              <a href={{ url('manage/report/excel').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no'] }} style="width:60px;font-weight: bold" class="btn btn-danger">Excel</a>
                                              <a href={{ url('manage/report/csv').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no']  }} style="width:60px;font-weight: bold" class="btn btn-info">Csv</a>
                                          </div>
           
                                      </div>
      
                  
                                            @endif    

                                            @if($TransectionDetail!=null)

                                      <div class="row" style="margin-top: 30px">
                                              <table class="table">
                                                      <tbody><tr>
                                                        <th>Trans ID</th>
                                                        <th>Date Time</th>
                                                        <th>Branch No</th>
                                                        <th>Branch Name</th>
                                                        <th>Maxcard No.</th>
                                                        <th>Product ID</th>
                                                        <th>Product Name</th>
                                                        <th>Type</th>
                                                        <th>Price</th>
                                                      </tr>
                                                      @foreach($TransectionDetail as $key=>$value)
                                                      <tr>
                                                        <td>{{ $value->id_trans }}</td>
                                                        <td>{{ $value->create_date }}</td>
                                                        <td>{{ $value->store_id }}</td>
                                                        <td>{{ $value->store_name }}</td>
                                                        <td>{{ $value->maxcard_id }}</td>
                                                        <td>{{ $value->pid }}</td>
                                                        <td>{{ $value->p_name }}</td>
                                                        <td>{{ $value->type_product_name }}</td>
                                                        <td>{{ number_format($value->price,2) }}</td>
                                                        <td>{{ $value->total_film }}</td>
                                            
                                                      </tr>
                                                      @endforeach
                                                    </tbody>
                                                  </table>
      
                                      </div>
      
                                      <div class="row" style="margin-top: 10px;margin-bottom: 20px">
      
                                              <div class="col-sm-10">
                                              <a href={{ url('manage/report/pdf') .'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no'] }} style="width:60px;font-weight: bold"  class="btn btn-success">Pdf</a>
                                              <a href={{ url('manage/report/excel').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no'] }} style="width:60px;font-weight: bold" class="btn btn-danger">Excel</a>
                                              <a href={{ url('manage/report/csv').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no']  }} style="width:60px;font-weight: bold" class="btn btn-info">Csv</a>
                                          </div>
           
                                      </div>
      
                  
                                            @endif    

                                            @if($SummaryEarn!=null)

                                            <div class="row" style="margin-top: 30px">
                                                    <table class="table">
                                                            <tbody><tr>
                                                              <th>No</th>
                                                              <th>Branch No</th>
                                                              <th>Branch Name</th>
                                                              <th>Total Point</th>
                                                              <?php 
                                                              $total = 0;
                                                              ?>
                                                            </tr>
                                                            @foreach($SummaryEarn as $key=>$value)
                                                            <tr>
                                                            <td>{{ $key+1 }}</td>
                                                              <td>{{ $value->store_id }}</td>
                                                              <td>{{ $value->store_name }}</td>
                                                              <td>{{ number_format($value->total,2) }}</td>
                                                              <?php
                                                                $total+= $value->total ;
                                                              ?>
                                                            </tr>
                                                            @endforeach
                                                          </tbody>
                                                          <tfoot>
                                                                <tr>
                                                                  <th style="text-align: right" id="total" colspan="3">Total :</th>
                                                                  <td>{{ number_format($total,2) }}</td>
                                                                </tr>
                                                               </tfoot>
                                                        </table>
            
                                            </div>
            
                                            <div class="row" style="margin-top: 10px;margin-bottom: 20px">
            
                                                    <div class="col-sm-10">
                                                    <a href={{ url('manage/report/pdf') .'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no'] }} style="width:60px;font-weight: bold"  class="btn btn-success">Pdf</a>
                                                    <a href={{ url('manage/report/excel').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no'] }} style="width:60px;font-weight: bold" class="btn btn-danger">Excel</a>
                                                    <a href={{ url('manage/report/csv').'/'.$input['report'].'/'.$input['store'].'/'.str_replace('/','^',str_replace(' - ','&&',$input['start_end'])).'/'.$input['maxcard_no']  }} style="width:60px;font-weight: bold" class="btn btn-info">Csv</a>
                                                </div>
                 
                                            </div>
            
                        
                                                  @endif    

                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              {{-- <a href="{{ url('manage/store') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" id="savebutton" class="btn btn-success pull-right">Save</button> --}}
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>

    <script>

          $(function () {
           $('input[name="start_end"]').daterangepicker({
               // startDate: moment().startOf('hour'),
               // endDate: moment().startOf('hour').add(32, 'hour'),
               locale: {
                //    format: 'DD/MM/YYYY hh:mm A'
               },
               "maxSpan": {
        "month": 2 
    },
           });
       });

       var input = {!! json_encode($input) !!}

    //    console.log(input.report);

       if($('#report_type').val()==="redeem"){
        input.report==="tredeem"?$('#report').append('<option value="tredeem" selected>Transaction Redeem</option>'):$('#report').append('<option value="tredeem">Transaction Redeem</option>');
        input.report==="sredeem"?$('#report').append('<option value="sredeem" selected>Summary Redeem</option>'):$('#report').append('<option value="sredeem">Summary Redeem</option>');
}
else if($('#report_type').val()==="earn"){
    input.report==="tearn"?$('#report').append('<option selected value="tearn">Transaction Earn Header</option>'):$('#report').append('<option value="tearn">Transaction Earn Header</option>');
    input.report==="dearn"?$('#report').append('<option selected value="dearn">Transaction Earn Detail</option>'):$('#report').append('<option value="dearn">Transaction Earn Detail</option>');
    input.report==="searn"?$('#report').append('<option selected value="searn">Summary Earn</option>'):$('#report').append('<option value="searn">Summary Earn</option>');
}

if($('#report').val()==="tearn" && $('#report_type').val()==="earn"){
    $('#maxcard').show();
}
else if($('#report').val()==="dearn" && $('#report_type').val()==="earn"){
    $('#maxcard').show();
}
// if($('#report').val()==="dearn" && $('#report_type').val()==="earn"){
//     $('#maxcard').show();
// }


 $('#report').on('change', function() { 
    if(this.value==="tearn" && $('#report_type').val()==="earn"){
    $('#maxcard').show();
}
else if(this.value==="dearn" && $('#report_type').val()==="earn"){
    $('#maxcard').show();
}
else{
    $('#maxcard').hide();
}

 })


       $('#report_type').on('change', function() {
        $('#report').empty();


if(this.value==="redeem"){
    input.report==="tredeem"?$('#report').append('<option value="tredeem" selected>Transaction Redeem</option>'):$('#report').append('<option value="tredeem">Transaction Redeem</option>');
        input.report==="sredeem"?$('#report').append('<option value="sredeem" selected>Summary Redeem</option>'):$('#report').append('<option value="sredeem">Summary Redeem</option>');
}
else if(this.value==="earn"){
    input.report==="tearn"?$('#report').append('<option selected value="tearn">Transaction Earn Header</option>'):$('#report').append('<option value="tearn">Transaction Earn Header</option>');
    input.report==="dearn"?$('#report').append('<option selected value="dearn">Transaction Earn Detail</option>'):$('#report').append('<option value="dearn">Transaction Earn Detail</option>');
    input.report==="searn"?$('#report').append('<option selected value="searn">Summary Earn</option>'):$('#report').append('<option value="searn">Summary Earn</option>');
}

        if($('#report').val()==="tearn" && this.value==="earn"){
    $('#maxcard').show();
}
else if($('#report').val()==="dearn" && this.value==="earn"){
    $('#maxcard').show();
}
else{
    $('#maxcard').hide();
}


 
});


    
            </script>
            
@stop