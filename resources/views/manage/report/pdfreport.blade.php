<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
</head>
<body>
        <div class="row" style="margin-top: 30px">
                <table class="table">
                        <tbody><tr>
                          <th>Date Time</th>
                          <th>Branch No</th>
                          <th>Branch Name</th>
                          <th>Maxcard No</th>
                          <th>Redeem Code</th>
                          <th>Redeem Desc</th>
                          <th>Point</th>
                        </tr>
                        <?php 
                        $total = 0;
                        ?>
                        @foreach($TranSectionRedeem as $key=>$value)
                        <tr>
                          <td>{{ $value->create_date }}</td>
                          <td>{{ $value['Store']->store_id }}</td>
                          <td>{{ $value['Store']->store_name }}</td>
                          <td>{{ $value->maxcard_id }}</td>
                          <td>{{ $value['Redeem']->rd_code }}</td>
                          <td>{{ $value['Redeem']->rd_desc }}</td>
                          <td>{{ $value['Redeem']->rd_point }}</td>
                          <?php
                            $total+= $value['Redeem']->rd_point ;
                          ?>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                            <tr>
                              <th style="text-align: right" id="total" colspan="6">Total :</th>
                              <td>{{ $total }}</td>
                            </tr>
                           </tfoot>
                    </table>

</body>
</html>