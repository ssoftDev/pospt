@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Group Product Management <small>ระบบจัดการ Group Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Group Product Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/group-template-product/') }}" method="POST" autocomplete="off">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->tmppd_id  }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">
                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Template Name</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input type="text" class="form-control" required id="t_name" name="t_name"  @if(isset($edit)) value="{{ $edit[0]->tmppd_name }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                        </div>
                                </div>
                              </div>


                                  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                      
                                        <div class="col-sm-10">
                                                <select class="form-control" name="businessunit" id="businessunit" @if(isset($edit) ) disabled @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                        @foreach($businessUnit as $key=>$value)
                                                            <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                        @endforeach
                                                        
                                                          </select>
                                              </div>
        
                                       
                                      </div>

                                      <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Group Product</label>
                      
                                        <div class="col-sm-6">
       
                                              <select name="gp_name" class="form-control"  id="gp_name"></select>
                                    
                                        </div>

                                        <div class="col-sm-2">
                                            <i class="fa fa-plus" style="font-size:30px"></i>
                                        </div>
                                      </div>

                                      <div class="row" style="margin-top:20px">
                            
                                        <div class="col-sm-12">

                                            <table id="groupTable" class="table table-bordered table-hover sorted_table">
                                                    <thead>
                                                <tr>
                                                    <th> Sequence   </th>
                                                    <th> Group Name   </th>
                                                    <th> View Product</th>
                                                    <th> Action   </th>
                                                </tr>
                                                    </thead>

                                                <tbody >
                                                    </tbody>

                                            </table>
                                        </div>
                

                                      </div>
        

                 <input type="hidden" name="dataTableGroup" id="tablegroup">

                              
                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/group-template-product') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->


  <div class="modal fade bs-example-modal-lg" id="modalViewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">View Product</h4>
            </div>
            <div class="modal-body" style="max-height: calc(100vh - 200px);
            overflow-y: auto;">
                    <table id="tableViewProduct" class="table table-bordered table-hover">
                            <thead>
                        <tr>
                            <th> No   </th>
                            <th> Product Name   </th>
                            <th> Price   </th>
                            <th> Remark </th>
                            <th width="20%"> Picture </th>
                        </tr>
                            </thead>

                        <tbody >
                            </tbody>

                    </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      
  
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')



    <script>      

    function onViewProduct(index,namegp){
        $('#modalViewProduct').modal('show');

        $('#myModalLabel').text('View Product Group ' +namegp)

        $.ajax({
        url: "{{ url('manage/group-template-product/getProduct') }}",
        method: "POST",
        data: {
            query: index,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            $('#tableViewProduct tbody').empty();

            data.data.map((value,index)=>{
                $('#tableViewProduct > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.product.p_name}</td><td>${value.product.p_price}</td><td>${value.product.remark}</td><td><img src="${value.product.img_url}" style="width:100%;" alt="" srcset=""></td></tr>`)
            })
            console.log(data)
            // $('#gp_name').append(htmldata);
        }
    });
    }

     

$(document).ready(function () {


    removegroup = function (index) {
        dataTable.splice(index, 1)
        $('#groupTable tbody').empty();

          $('#gp_name').empty();
        $.ajax({
        url: "{{ url('manage/group-template-product/getGroupProduct') }}",
        method: "POST",
        data: {
            query: $('#businessunit').val(),
            data: dataTable,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            // console.log(data)
            var htmldata = $(data);

             if(data===""){
                $('#gp_name').attr('disabled',true);

            }else{
                $('#gp_name').attr('disabled',false);
                $('#gp_name').append(data);
            }

             if($('#redegp_namem').val()!==null){
                $('#gp_name').prop('disabled', false);
            }
            
            // $('#gp_name').append(htmldata);
        }
    });
   

        dataTable.map((value, index) =>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td> <td><button type="button" style="width:60px" onclick="onViewProduct(${value.id},'${value.group_name}')" class="btn btn-block btn-primary btn-flat">view</button></td> <td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
        )

        $('#tablegroup').val(JSON.stringify(dataTable))

        if(dataTable.length===0){
                $('#tablegroup').val("")
                console.log("hello")
            }
    }


    var idAddGP;
    var GPname;


    var query = $('#businessunit').val();

    console.log(query)

      var dataTable = [];

var edit = {!! isset($edit) ? json_encode($mpProductTem) : "null" !!};


if (edit !== null) {
    //    console.log(edit)

    var groupProduct = {!!  isset($groupProduct) ? json_encode($groupProduct) : "null" !!}


    edit.map((value, index) => {

        groupProduct.map((data, indexg) => {
            if (data.gp_id == value.gp_id) {
                dataTable.push({
                    id: value.gp_id,
                    group_name: data.gp_name
                })
            }
        })

    })


    dataTable.map((value, index) =>
        $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td> <td><button type="button" style="width:60px" onclick="onViewProduct(${value.id},'${value.group_name}')" class="btn btn-block btn-primary btn-flat">view</button></td> <td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
    )
    $('#tablegroup').val(JSON.stringify(dataTable))
}


    $.ajax({
        url: "{{ url('manage/group-template-product/getGroupProduct') }}",
        method: "POST",
        data: {
            query: query,
            data: dataTable,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            console.log(query)
            var htmldata = $(data);
            htmldata.each(function(){
                dataTable.map((val)=>{
                    if(val.group_name===$(this).attr('name')){
                        $(this).remove()
                    }
                })
                console.log($(this).attr('name'))
            })


            $('#gp_name').append(htmldata);
            if($('#gp_name').val()===null){
                $('#gp_name').prop('disabled', 'disabled');
            }
        }
    });


     

    $('#businessunit').on('change', function () {

        dataTable = [];

          $('#groupTable tbody').empty();


        $('#tablegroup').val(JSON.stringify(dataTable))

        $('#gp_name').empty();
        $.ajax({
        url: "{{ url('manage/group-template-product/getGroupProduct') }}",
        method: "POST",
        data: {
            query: this.value,
            data: dataTable,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            // console.log(data)
            var htmldata = $(data);

             if(data===""){
                $('#gp_name').attr('disabled',true);

            }else{
                $('#gp_name').attr('disabled',false);
                $('#gp_name').append(data);
            }
            
            // $('#gp_name').append(htmldata);
        }
    });
    });

    $('#gp_name').on('change',function(){
        idAddGP = this.value
        GPname = $("#gp_name option:selected").text()
    });



    $('tbody').sortable({
        start: function (event, ui) {
            ui.item.startPos = ui.item.index();
        },
        stop: function (event, ui) {
            console.log("Start position: " + ui.item.startPos);
            console.log("New position: " + ui.item.index());

            var temp = dataTable[ui.item.startPos];

            dataTable[ui.item.startPos] = dataTable[ui.item.index()]
            dataTable[ui.item.index()] = temp;

            $('#groupTable tbody').empty();


            dataTable.map((value, index) =>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td> <td><button type="button" style="width:60px" onclick="onViewProduct(${value.id},'${value.group_name}')" class="btn btn-block btn-primary btn-flat">view</button></td> <td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
        )
            $('#tablegroup').val(JSON.stringify(dataTable))


        }
    });



  


    



    $(document).on('click', 'i', function () {

 
        if($('#gp_name').val()===null){
            alert("Select Group Product");
        }else{
            dataTable.push({
            id: $('#gp_name').val(),
            group_name: $("#gp_name option:selected").text()
        })

        $("#gp_name option:selected").remove();


        $('#groupTable tbody').empty();
        // $('#gp_name').val()

           if($('#gp_name').val()===null){
                $('#gp_name').prop('disabled', 'disabled');
            }


         dataTable.map((value, index) =>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td> <td><button type="button" style="width:60px" onclick="onViewProduct(${value.id},'${value.group_name}')" class="btn btn-block btn-primary btn-flat">view</button></td> <td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
        )
        $('#tablegroup').val(JSON.stringify(dataTable))
        }

     

    })


    $(document).on('click', 'li', function () {
        $('#gp_name').val($(this).text());
        idAddGP = $(this).attr('id')

        $('#groupProductList').fadeOut();
    });

});
    </script>

    <script>







$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop