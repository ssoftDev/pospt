@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Redeem Management <small>ระบบจัดการ Redeem</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Redeem Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if (flashMe()->ok())
        {!! flashMe_flash() !!}
      @endif
        <form id="deleteUserForm" action={{ url('manage/redeem/') }} method="get">
          <div class="row">
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="text" value="{!! \Request::get('q') !!}" name="q" class="form-control">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>
  
              <div class="col-sm-4">
                  <button type="submit"  style="width:70px" class="btn btn-block btn-success btn-flat" >ค้นหา</button>
          </div>
            </div>
  
          
        </form>


            <div class="row" style="margin-top:20px">
                    <div class="col-sm-4">
                            <a href="{{ url('manage/redeem/getform') }}"  style="width:70px" class="btn btn-block btn-success btn-flat">เพิ่ม</a>
                    </div>
                </div>
                           
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-12">
                            <table class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 10px">No</th>
                                      <th>Code</th>
                                      <th>Description</th>
                                      <th>Business Unit</th>
                                      <th>Point</th>
                                      <th>Status</th>
                                      <th>Discount</th>
                                      <th>Discount Price</th>
                                      <th>Created Date</th>
                                      <th>Created By</th>
                                      <th>Updated Date</th>
                                      <th>Updated By</th>
                                      <th width="150px">Action</th>
 
                                    </tr>
                                    @foreach($Redeem as $index=>$value)
                                    <tr>
                                        <td>{{ (($index+1)+($Redeem->currentPage()*$Redeem->perPage()))-10 }}</td>
                                      <td>{{ $value->rd_code }}</td>
                                      <td>{{ $value->rd_desc }}</td>
                                      <td>{{ $value->BusinessUnit['bu_name'] }}</td>
                                      <td>{{ number_format($value->rd_point,2) }}</td>
                                      <td>{!! $value->is_redeem=='y'?'Public':'Private' !!}</td>
                                      <td>{!! $value->is_discount=='y'?'Yes':'No' !!}</td>
                                      <td>{{ number_format($value->discount_price,2) }}</td>
                                      <td>{{ $value->create_date }}</td>
                                      <td>{{ $value->create_by }}</td>
                                      <td>{{ $value->update_date }}</td>
                                      <td>{{ $value->update_by }}</td>
                                      <td><div class="row" >
                                          <div class="col-sm-5">
                                          <a href="{{ url('manage/redeem/edit').'/'.$value->rd_id }}" style="width:60px" class="btn btn-block btn-warning btn-flat">แก้ไข</a>
                                          </div>
                                          <div class="col-sm-5">
                                              <button type="button" style="width:60px" class="btn btn-block btn-danger btn-flat" onclick="ShowDelete('{{$value->rd_id}}')">ลบ</button>
                                          </div>
                                        </div></td>
        
                                    </tr>
                                    @endforeach
                                 
                                  </tbody></table>
                    </div>
                </div>
        
                          
                          
                <div class="box-footer clearfix">
                    <div class="pull-left">Total : {{ $Redeem->total() }}</div>
  
                          <ul class="pagination pagination-sm no-margin pull-right">
                          {{ $Redeem->links() }}
                          </ul>
                        </div>
    </div><!-- /.box-body -->

  </div><!-- /.box -->

  <div class="modal fade" id="modelDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">เตือน</h4>
            </div>
            <div class="modal-body">
              Do you want to delete this Redeem?
            </div>
            <form id="deleteUserForm" method="POST" action="{{ url("manage/redeem/delete") }}">

                {{ csrf_field() }}
                <input type="hidden" name="iddelete" id="iddelete">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
              <button type="submit" class="btn btn-primary">OK</button>
              
            </div>
          </form>
          </div>
        </div>
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> 
        function ShowDelete(id){
          let url = {!! json_encode(url("manage/redeem/delete")) !!}
          console.log(id,url)
          $('#modelDelete').modal('show') 
      $('#iddelete').val(id)
        }
        </script>
@stop