@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Redeem Management <small>ระบบจัดการ Redeem</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Redeem Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/redeem/') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->rd_id  }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">
                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Code</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input required type="text" class="form-control" id="rd_code" name="rd_code"  @if(isset($edit)) value="{{ $edit[0]->rd_code }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                                      </div>
                                </div>
                              </div>

                              <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                  
                                    <div class="col-sm-10">
                                            <select class="form-control" name="businessunit" @if(isset($edit) ) disabled @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                    @foreach($businessUnit as $key=>$value)
                                                        <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                    @endforeach
                                                    
                                                      </select>
                                          </div>
    
                                   
                                  </div>

                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Description</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input type="text" required class="form-control" id="rd_desc" name="rd_desc"  @if(isset($edit)) value="{{ $edit[0]->rd_desc }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                </div>
                                </div>
                              </div>

                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Point</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input required type="number" class="form-control" id="rd_point" style="text-align: right" name="rd_point"  @if(isset($edit)) value="{{ $edit[0]->rd_point }}" @else value=0 @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                </div>
                                </div>

                              </div>

                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Discount</label>
              
                                <div class="col-sm-10">
                  
                                        <label class="radio-inline"><input type="radio" id="discount" name="discount" value="y" @if(isset($edit) && $edit[0]->is_discount=='y') checked @endif>Yes</label>
                                        <label class="radio-inline"><input type="radio" id="discount" name="discount" value="n" @if(isset($edit) && $edit[0]->is_discount=='n') checked  @elseif(!isset($edit) ) checked   @endif>No</label>

       
                                </div>


                                <div class="col-sm-2">
                                  </div>

                                <div class="col-sm-10" style="margin-top:20px">
                                    <div class="input-group">
                                        <input id="price" type="number" name="discount_price" style="text-align: right"  min="0"  step="any"  class="form-control" @if(isset($edit)) value="{{ $edit[0]->discount_price }}" @endif>
                                        <span class="input-group-addon"><b>Bath</b></span>
                                      </div>
                                </div>

                                  

                              </div>

                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Status</label>
              
                                <div class="col-sm-10">
                  
                                        <label class="radio-inline"><input type="radio" name="status" value="y" @if(isset($edit) && $edit[0]->is_redeem=='y') checked @elseif(!isset($edit) ) checked @endif>Public</label>
                                        <label class="radio-inline"><input type="radio" name="status" value="n" @if(isset($edit) && $edit[0]->is_redeem=='n') checked  @endif>Private</label>

                                </div>
                                
                              </div>

                              <div class="form-group">
                                  
                                    <label for="inputEmail3" class="col-sm-2 control-label pull-left">Image</label>
                  
                                    <div class="col-sm-10">
                                            <input type="file" name="redeempic" id="imgInp" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                                            <img id="blah" src="#" alt="your image" style="max-width: 200px;margin-top: 20px"/>

                                            

                                            
                                            
                                            

                                    </div>
                                  </div>
                                  @if(isset($edit) && $edit[0]->img_url) 
                                  <div class="form-group">
                          
                                    <label for="inputEmail3" class="col-sm-2 control-label pull-left">Now Image</label>
                  
                                    <div class="col-sm-10">
                                        <img id="blah" src="{{ $edit[0]->img_url}}" alt="your image" style="max-width: 200px;margin-top: 20px"/>
                                    </div>
                                  </div>
                                  @endif





                            
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/redeem') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')



    <script>      

        if($('#discount').is(':checked')){
            $('#price').attr('readonly', false);
        }else{
            $('#price').attr('readonly', true);
            $('#price').val("0.00");
        }
        
        
      
        
     
     $('input[type=radio][name=discount]').on('change',function() {

         console.log(this.value)
    if (this.value == 'y') {
        $('#price').attr('readonly', false);
    }
    else if (this.value == 'n') {
        $('#price').val("0.00");
        $('#price').attr('readonly', true);
    }
});


    $(document).ready(function () {


        removegroup = function(index){
            dataTable.splice(index,1)
            $('#groupTable tbody').empty();
        

         
            dataTable.map((value,index)=>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )
        }


        var idAddGP;

        $('#gp_name').keyup(function () {
            var query = $(this).val();
            if (query != '') {

                $.ajax({
                    url: "{{ url('manage/group-template-product/getGroupProduct') }}",
                    method: "POST",
                    data: {
                        query: query,
                        data:dataTable,
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        
                    },
                    success: function (data) {
                        console.log(dataTable)
                        var htmldata = $(data);

                        htmldata.find('li').each(function(i,val){
                            dataTable.map((value,index)=>{
                                if($(this).attr('id')==value.id){
                                    $(this).remove();
                                }
                                console.log(value)
                            })

                        })
                        $('#groupProductList').fadeIn();
                        $('#groupProductList').html(htmldata);
                    }
                });
            }
        });

              $('tbody').sortable({
    start: function(event, ui) {
        ui.item.startPos = ui.item.index();
    },
    stop: function(event, ui) {
        console.log("Start position: " + ui.item.startPos);
        console.log("New position: " + ui.item.index());

        var temp = dataTable[ui.item.startPos];

         dataTable[ui.item.startPos] = dataTable[ui.item.index()]
        dataTable[ui.item.index()] = temp;

          $('#groupTable tbody').empty();

         
            dataTable.map((value,index)=>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )

            $('#tablegroup').val(dataTable)
       

    }
});

        var dataTable = [];

        $(document).on('click', 'i', function () {

            dataTable.push({
                id: idAddGP,
                group_name: $('#gp_name').val()
            })



            $('#groupTable tbody').empty();
            $('#gp_name').val("")

         
            dataTable.map((value,index)=>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.group_name}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )
            $('#tablegroup').val(JSON.stringify(dataTable))
            
        })


        $(document).on('click', 'li', function () {
            $('#gp_name').val($(this).text());
            idAddGP = $(this).attr('id')

            $('#groupProductList').fadeOut();
        });

    }); 
    
    
    </script>

    <script>







$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop