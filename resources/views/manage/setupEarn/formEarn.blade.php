@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Setup Earn Management <small>ระบบจัดการ Setup Earn  Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Setup Earn  Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/setup-earn/') }}" method="POST" autocomplete="off">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $id }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">
                                  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                      
                                        <div class="col-sm-10">
                                                <select class="form-control" name="businessunit" id="bu_id" @if(isset($edit) ) disabled @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                        @foreach($businessUnit as $key=>$value)
                                                            <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                        @endforeach
                                                        
                                                          </select>
                                              </div>
        
                                       
                                      </div>

                                      <div class="form-group">
                                  
                                            <label for="inputEmail3" class="col-sm-2 control-label pull-left">Check SKU</label>
                          
                                            <div class="col-sm-10">
                              
                                                    <label class="radio-inline"><input type="radio" id="sku" name="sku" value="y" >Yes</label>
                                                    <label class="radio-inline"><input type="radio" id="sku" name="sku" value="n"  checked >No</label>
            
                                            </div>
            
            
                                            <div class="col-sm-2">
                                              </div>
            
                                            <div class="col-sm-10" style="margin-top:20px" id="productdiv">
                                                <div class="input-group">
                                                    {{-- <input id="product" type="text" id="product" name="product" class="form-control" @if(isset($edit)) value="{{ $edit[0]->discount_price }}" @endif>
                                                    <div id="ProductList" style="position: absolute">
                                                    </div> --}}

                                                      <select class="js-example-basic-single" name="product" id="product" value="{{ old("product") }}">
                                                            {{-- @foreach($businessUnit as $key=>$value)
                                                                <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected   @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                            @endforeach --}}

                                                            <?php
                                                            $store = null;
                                                            if(trim(\Auth::user()->group_user_id)==1){
                                                                if(Session::get('bu')!='all'){
                                                                    $store = DB::table('product')
                                                                   ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                                                                   ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                                                                   ->where('group_product.bu_id', Session::get('bu'))
                                                                    ->get();
                                                                }else{
                                                                      $store = DB::table('product')
                                                                   ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                                                                   ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                                                                    ->get();
                                                                }
                                                            }else{
                                                                $store = DB::table('product')
                                                                   ->join('mp_product', 'product.pid', '=', 'mp_product.pid')
                                                                   ->join('group_product', 'group_product.gp_id', '=', 'group_product.gp_id')
                                                                   ->where('group_product.bu_id', trim(\Auth::user()->bu_id))
                                                                    ->get();
                                                            }

                                                           
                                                                foreach ($store as $key => $value) {
                                                                        if(isset($edit) && $edit[0]->pid==$value->pid){
                                                                         echo("<option value='{$value->pid}'>{$value->p_name}</option>");
                                                                    }else{
                                                                    echo("<option value='{$value->pid}'>{$value->p_name}</option>");
                                                                    }
                                                                }
                                                            ?>
                                                            
                                                              </select>

                                                    <span class="input-group-addon"><b>Product</b></span>
                                                  </div>
                                            </div>
            
                                              
            
                                          </div>

                                      <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Price</label>
                      
                                        <div class="col-sm-3">
                                                <div class="input-group">
                                                        <input type="number" class="form-control" style="text-align: right" min="0" id="price" name="price" required  value=0 >
                                                        <span class="input-group-addon"><b>Bath</b></span>
                                                      </div>
                                          
                                        </div>

                     

                                        <div class="col-sm-3">
                                                <div class="input-group">
                                                        <input type="number" style="text-align: right" min="0" class="form-control" id="per_point" name="per_point" required value=0 >
                                                        <span class="input-group-addon"><b>Point</b></span>
                                                      </div>
                                            
                                          </div>
  
                
                                      </div>

                                      <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Multiply</label>
                      
                                        <div class="col-sm-3">
                                                <div class="input-group">
                                                        <input type="number" style="text-align: right" min="0" class="form-control" id="multiply" name="multiply" required  value=1>
                                                        <span style="width: 53px;" class="input-group-addon"><b>X</b></span>
                                                      </div>
                                          
                                        </div>
                                        


                                    
                                      </div>

                                      

                                      <div class="form-group">

                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Start Date - End Date</label>
                        
                                        <div class="col-sm-10">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                          </div>
                                          <input type="text" name="start_end" id="start_end" class="form-control pull-right" />
                                        </div>
                                        <!-- /.input group -->
                                      </div>
                                    </div>

                                    <div class="form-group">

                                            <div class="col-sm-2"></div>
                            
                                            <div class="col-sm-1">
                                                    <button type="button" id="addSeq" style="width:56px" class="btn btn-success pull-right">Add</button>
                                            <!-- /.input group -->
                                          </div>
                                        </div>




                                      <div class="row" style="margin-top:20px">
                            
                                        <div class="col-sm-12">

                                            <table id="groupTable" class="table table-bordered table-hover sorted_table">
                                                    <thead>
                                                <tr>
                                                    <th> Sequence</th>
                                                    <th> Amount  </th>
                                                    <th> Point   </th>
                                                    <th> Multiply</th>
                                                    <th> SKU     </th>
                                                    <th> Product ID</th>
                                                    <th> Start Date</th>
                                                    <th> End Date</th>
                                                    <th> Action  </th>
                                                </tr>
                                                    </thead>

                                                <tbody >
                                                    </tbody>

                                            </table>
                                        </div>
                

                                      </div>
        

                 <input type="hidden" name="tableearn" id="tableearn">

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/setup-earn') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
  
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')




    <script>      

     $('.js-example-basic-single').select2({ 
    width: '100%'});

    
    var dataTable = [];

       if ($('#sku').is(':checked')) {
           $('#productdiv').show();
           $('#price').attr('disabled', true)
           $('#per_point').attr('disabled', true)
           $('#multiply').attr('disabled', false)
       } else {
           $('#productdiv').hide();
           $('#price').attr('disabled', false)
           $('#per_point').attr('disabled', false)
           $('#multiply').attr('disabled', true)

       }

       $('input[type=radio][name=sku]').on('change', function () {

           console.log(this.value)
           if (this.value == 'y') {
               $('#productdiv').show();
               $('#productdiv').show();
               $('#price').attr('disabled', true)
               $('#per_point').attr('disabled', true)
               $('#multiply').attr('disabled', false)

               $('#price').val(0)
               $('#per_point').val(0)
               $('#multiply').val(1)
           } else if (this.value == 'n') {
               $('#productdiv').val(null);
               $('#price').attr('disabled', false)
               $('#per_point').attr('disabled', false)
               $('#multiply').attr('disabled', false)
                dataTable.length===0?null:$('#price').val(1)
               dataTable.length===0?null:$('#per_point').val(1)
               $('#multiply').val(1)

               console.log(dataTable.length)

               dataTable.length===0?$('#multiply').attr('disabled', true):$('#multiply').attr('disabled', false);

               $('#productdiv').hide();
           }
       });


       $(function () {
           $('input[name="start_end"]').daterangepicker({
               // startDate: moment().startOf('hour'),
               // endDate: moment().startOf('hour').add(32, 'hour'),
               locale: {
                //    format: 'DD/MM/YYYY hh:mm A'
               }
           });
       });

       // $('input[name="datetimes"]').daterangepicker().on('changeDate', function(e) {
       //     alert('date has changed!');
       // });

       $('input[name="start_end"]').datepicker()
           .on("input change", function (e) {
               console.log(e.target.value);

           });


       $('input[name="start_end"]').on('focus blur click', function () {
           $("#ui-datepicker-div").hide();

       });


       $(document).ready(function () {


           removegroup = function (index) {
               if(index===0){
                   alert("ไม่สามารถลบได้")
                    return 0;
               }
               dataTable.splice(index, 1)
               $('#groupTable tbody').empty();



            dataTable.map((value, index) =>
$('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.amount}</td><td>${value.per_point}</td><td>${value.multiply}</td><td>${value.sku}</td><td>${value.pid===null||value.pid===undefined?' - ':value.pid}</td><td>${value.start_time}</td><td>${value.end_time}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
)
               $('#tableearn').val(JSON.stringify(dataTable))

               if (dataTable.length === 0) {
                   $('#tablegroup').val("")
               }
           }


           var pid;
           var rdcode;

           $('#product').keyup(function () {
               var query = $(this).val();
               if (query != '') {

                   $.ajax({
                       url: "{{ url('manage/setup-earn/getProduct') }}",
                       method: "POST",
                       data: {
                           query: query,
                           data: dataTable,
                           _token: $('meta[name="csrf-token"]').attr('content'),

                       },
                       success: function (data) {
                           console.log(dataTable)
                           var htmldata = $(data);

                           htmldata.find('li').each(function (i, val) {
                               dataTable.map((value, index) => {
                                   if ($(this).attr('id') == value.id) {
                                       $(this).remove();
                                   }
                                   console.log(value)
                               })

                           })
                           $('#ProductList').fadeIn();
                           $('#ProductList').html(htmldata);
                       }
                   });
               }
           });

           $('tbody').sortable({
               start: function (event, ui) {
                   ui.item.startPos = ui.item.index();
               },
               stop: function (event, ui) {
                   console.log("Start position: " + ui.item.startPos);
                   console.log("New position: " + ui.item.index());

                   if(ui.item.index()===0 ||ui.item.startPos===0 ){
                       alert("ไม่สามารถสลับอันดับแรกได้")
                       $('#groupTable tbody').empty();


  dataTable.map((value, index) =>
$('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.amount}</td><td>${value.per_point}</td><td>${value.multiply}</td><td>${value.sku}</td><td>${value.pid===null||value.pid===undefined?' - ':value.pid}</td><td>${value.start_time}</td><td>${value.end_time}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
)
                   }else{
                       
                   var temp = dataTable[ui.item.startPos];
                   

                   dataTable[ui.item.startPos] = dataTable[ui.item.index()]
                   dataTable[ui.item.index()] = temp;

                   $('#groupTable tbody').empty();


        dataTable.map((value, index) =>
$('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.amount}</td><td>${value.per_point}</td><td>${value.multiply}</td><td>${value.sku}</td><td>${value.pid===null||value.pid===undefined?' - ':value.pid}</td><td>${value.start_time}</td><td>${value.end_time}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
)
               $('#tableearn').val(JSON.stringify(dataTable))
                   }

           }
           });




           var edit = {!! isset($edit) ? json_encode($edit) : "null" !!};


           if (edit !== null) {
               //    console.log(edit)

               var Earn = {!! isset($edit) ? json_encode($edit) : "null" !!}
               var mpEarn = {!! isset($edit) ? json_encode($MP_Earn) : "null"!!}

               console.log(mpEarn)


               Earn.map((value, index) => {

                   if (value.is_check === 'y') {
                       mpEarn.map((val, key) => {
                           if (value.eid === val.eid) {
                               dataTable.push({
                                   bu_id: value.bu_id,
                                   amount: value.bath,
                                   per_point: value.point,
                                   multiply: value.multiply,
                                   sku: value.is_check,
                                   start_end: moment(value.start_date).format('MM/DD/YYYY') + ' - ' + moment(value.expire_date).format('MM/DD/YYYY'),
                                   pid: val.pid,
                                 start_time:moment(value.start_date).format('MM/DD/YYYY'),
                                   end_time:moment(value.expire_date).format('MM/DD/YYYY')
                               })
                           }
                       })
                   } else {
                       dataTable.push({
                           bu_id: value.bu_id,
                           amount: value.bath,
                           per_point: value.point,
                           multiply: value.multiply,
                           sku: value.is_check,
                           start_end: moment(value.start_date).format('MM/DD/YYYY') + ' - ' + moment(value.expire_date).format('MM/DD/YYYY'),
                           pid: null,
                           start_time:moment(value.start_date).format('MM/DD/YYYY'),
                                   end_time:moment(value.expire_date).format('MM/DD/YYYY')
                       })
                   }




               })
               console.log(dataTable)

 dataTable.length===0?$('#multiply').attr('disabled', true):$('#multiply').attr('disabled', false);



            
             dataTable.map((value, index) =>
$('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.amount}</td><td>${value.per_point}</td><td>${value.multiply}</td><td>${value.sku}</td><td>${value.pid===null||value.pid===undefined?' - ':value.pid}</td><td>${value.start_time}</td><td>${value.end_time}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
)
               $('#tableearn').val(JSON.stringify(dataTable))
           }


           var dataSku = [];



           $(document).on('click', '#addSeq', function () {

               var split = $('#start_end').val().split(' - ')
               var startTime =split[0]
               var endTime = split[1]
               

                // console.log(startTime.format())

               if ($('#sku').is(':checked')) {
                   if ($('#product').val() === "") {
                       alert('Add Product');
                       

                   } else {
                       dataTable.push({
                           bu_id: $('#bu_id').val(),
                           amount: $('#price').val(),
                           per_point: $('#per_point').val(),
                           multiply: $('#multiply').val(),
                           sku: $('#sku').is(':checked') ? 'y' : 'n',
                           pid: $('#sku').val() === 'y' ? $('#product').val() : null,
                           start_end: $('#start_end').val(),
                           start_time:startTime,
                           end_time:endTime
                       })
                       $('#price').val(1)
                       $('#per_point').val(1)
                       $('#multiply').val(1)
                       $('#product').val("")
                   }
               } else {
                   dataTable.push({
                       bu_id: $('#bu_id').val(),
                       amount: $('#price').val(),
                       per_point: $('#per_point').val(),
                       multiply: $('#multiply').val(),
                       sku: $('#sku').is(':checked') ? 'y' : 'n',
                       pid: $('#sku').val() === 'y' ? $('#product').val() : null,
                       start_end: $('#start_end').val(),
                       start_time:startTime,
                           end_time:endTime
                   })
                   $('#price').val(1)
                   $('#per_point').val(1)
                   $('#multiply').val(1)
                   $('#product').val("")
               }




               // $('#bu_id').val(""),

               // $('#sku').val("n")

               //    $("input[type=radio][name=sku][value=]").attr("checked","checked");

               $('input[name="sku"][value="n"]').prop('checked', true);
               $('#price').val(1)
                       $('#per_point').val(1)
                       $('#multiply').val(1)
                       $('#price').attr('disabled', false)
               $('#per_point').attr('disabled', false)
               $('#multiply').attr('disabled', false)

               $('#productdiv').hide();


               console.log(dataTable);



               $('#groupTable tbody').empty();
               $('#redeem').val("")


             dataTable.map((value, index) =>
$('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.amount}</td><td>${value.per_point}</td><td>${value.multiply}</td><td>${value.sku}</td><td>${value.pid===null||value.pid===undefined?' - ':value.pid}</td><td>${value.start_time}</td><td>${value.end_time}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
)
               $('#tableearn').val(JSON.stringify(dataTable))

           })


           $(document).on('click', 'li', function () {
               $('#product').val($(this).text());
               pid = $(this).attr('id')


               $('#ProductList').fadeOut();
           });

       });
    
    </script>

    <script>







$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop