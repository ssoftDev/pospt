@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Setup Earn Management <small>ระบบจัดการ Setup Earn </small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Setup Earn  Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if (flashMe()->ok())
        {!! flashMe_flash() !!}
      @endif
        <form id="deleteUserForm" action={{ url('manage/setup-earn/') }} method="get">
          <div class="row">
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="text" value="{!! \Request::get('q') !!}" name="q" class="form-control">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>
  
              <div class="col-sm-4">
                  <button type="submit"  style="width:70px" class="btn btn-block btn-success btn-flat" >ค้นหา</button>
          </div>
            </div>
  
          
        </form>


            <div class="row" style="margin-top:20px">
                    <div class="col-sm-4">
                            <a href="{{ url('manage/setup-earn/getform') }}"  style="width:70px" class="btn btn-block btn-success btn-flat" >เพิ่ม</a>
                    </div>
                </div>
                           
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-12">
                            <table class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 10px">No</th>
                                      
                                      <th>Business Unit</th>

                                      <th>View</th>
                               
                                      <th width="150px">Action</th>
 
                                    </tr>
                                    @foreach($SetupEarn as $index=>$value)
                                     @if($value->BusinessUnit['bu_name']!=null )
                                    <tr>
                                        <td>{{ (($index+1)+($SetupEarn->currentPage()*$SetupEarn->perPage()))-10 }}</td>
                                      <td>{{ $value->BusinessUnit['bu_name'] }}</td>
                                    <td><button type="button" style="width:60px" onclick="onViewEarn('{{ $value->bu_id }}','{{ $value->BusinessUnit['bu_name']  }}')" class="btn btn-block btn-primary btn-flat">view</button></td>
                                      <td><div class="row" >
                                          <div class="col-sm-5">
                                          <a href="{{ url('manage/setup-earn/edit').'/'.$value->bu_id }}" style="width:60px" class="btn btn-block btn-warning btn-flat">แก้ไข</a>
                                          </div>
                                          <div class="col-sm-5">
                                              <button type="button" style="width:60px" class="btn btn-block btn-danger btn-flat" onclick="ShowDelete('{{ $value->bu_id }}')">ลบ</button>
                                          </div>
                                        </div></td>
        
                                    </tr>
                                    @endif
                                    @endforeach
                                 
                                  </tbody></table>
                    </div>
                </div>
        
                          
                <div class="box-footer clearfix">
                    <div class="pull-left">Total : {{ $SetupEarn->total() }}</div>
  
                          <ul class="pagination pagination-sm no-margin pull-right">
                          {{ $SetupEarn->links() }}
                          </ul>
                        </div>
    </div><!-- /.box-body -->

  </div><!-- /.box -->

  <div class="modal fade" id="modelDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">เตือน</h4>
            </div>
            <div class="modal-body">
              Do you want to delete this Redeem Template ?
            </div>
            <form id="deleteUserForm" action="setup-earn/delete" method="POST">
                {{-- {{ method_field("DELETE") }} --}}
                {{ csrf_field() }}
                <input type="hidden" name="iddelete" id="iddelete">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
              <button type="submit" class="btn btn-primary">OK</button>
              
            </div>
          </form>
          </div>
        </div>
      </div>


      <div class="modal fade bs-example-modal-lg" id="modalViewEarn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="labelTitle">View Earn</h4>
            </div>
            <div class="modal-body" style="max-height: calc(100vh - 200px);
            overflow-y: auto;">
                    <table id="tableViewEarn" class="table table-bordered table-hover">
                            <thead>
                        <tr>
                            <th> Sequence   </th>
                            <th> Amount   </th>
                            <th> Point  </th>
                            <th> Multiply </th>
                            <th> SKU</th>
                            <th> Product ID</th>
                        </tr>
                            </thead>

                        <tbody >
                            </tbody>

                    </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> 
        function ShowDelete(id){
          let url = {!! json_encode(url("manage/redeem-template")) !!}
          console.log(url+'/'+id)
          $('#modelDelete').modal('show') 
          // $('#deleteUserForm').attr('action',url+'/'+id);
          $('#iddelete').val(id)
        }


        function onViewEarn(id,namebu){

          console.log(namebu)
          $('#modalViewEarn').modal('show');
          $('#labelTitle').text('View Earn ' +namebu)

            $.ajax({
        url: "{{ url('manage/setup-earn/getEarn') }}",
        method: "POST",
        data: {
            query: id,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            $('#tableViewEarn tbody').empty();

            data.data.map((value,index)=>{
              
              if(value.m_p!==null){
                $('#tableViewEarn > tbody:last-child').append(`<tr><td>${value.seq}</td><td>${value.bath}</td><td>${value.point}</td><td>${value.multiply}</td><td>${value.is_check}</td><td>${value.m_p.pid}</td></tr>`)
              }else{
                $('#tableViewEarn > tbody:last-child').append(`<tr><td>${value.seq}</td><td>${value.bath}</td><td>${value.point}</td><td>${value.multiply}</td><td>${value.is_check}</td><td> - </td></tr>`)
              }
                
            })
            // $('#gp_name').append(htmldata);
        }
    });
        }
        </script>
@stop