@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Business Unit Management <small>ระบบจัดการ Business Unit</small></h1>
    
@stop

@section('content')

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="exampleModalLabel">Add Business Unit</h4>
            </div>
            <div class="modal-body">
              <form action="{{ url('manage/business-unit/') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="recipient-name" class="control-label">Business Unit Name:</label>
                  <input type="text" class="form-control" name="bu_name">
                </div>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
          </div>
        </div>
      </div>


      <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Edit Business Unit</h4>
              </div>
              <div class="modal-body">
                <form action="{{ url('manage/business-unit') }}"  method="POST" id="editform">
                    {{ method_field('PUT') }} 
                  {{ csrf_field() }}
                  <input type="hidden" name="id" id="idedit">
                  <div class="form-group">
                    <label for="recipient-name" class="control-label">Business Unit Name:</label>
                    <input type="text" class="form-control" name="bu_name_edit" id="bu_name_edit">
                  </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
          </form>
            </div>
          </div>
        </div>


<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Business Unit Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if (flashMe()->ok())
        {!! flashMe_flash() !!}
      @endif
        <form id="deleteUserForm" action={{ url('manage/business-unit') }} method="get">
          <div class="row">
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="text" value="{!! \Request::get('q') !!}" name="q" class="form-control">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>
  
              <div class="col-sm-4">
                  <button type="submit"  style="width:70px" class="btn btn-block btn-success btn-flat" >ค้นหา</button>
          </div>
            </div>
  
          
        </form>
        
            <div class="row" style="margin-top:20px">
                    <div class="col-sm-4">
                            <button data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" style="width:70px" class="btn btn-block btn-success btn-flat" >เพิ่ม</button>
                    </div>
                </div>
                           
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-12">
                            <table class="table table-bordered">
                                    <tbody><tr>
                                      <th>No</th>
                                      <th>BU Name</th>
                                      <th width="150px">Action</th>
                                    </tr>
                                    @foreach($businessUnit as $index=>$value)
                                    <tr>
                                        <td>{{ (($index+1)+($businessUnit->currentPage()*$businessUnit->perPage()))-10 }}</td>
                                      <td>{{ $value->bu_name }}</td>
                                      <td>  <div class="row" >
                                  <div class="col-sm-5">
                                  <button onclick="ShowEdit('{{ $value->bu_name  }}','{{ $value->bu_id  }}')" style="width:60px" class="btn btn-block btn-warning btn-flat">แก้ไข</a>
                                  </div>
                                  <div class="col-sm-5">
                                      <button type="button" style="width:60px" class="btn btn-block btn-danger btn-flat" onclick="ShowDelete('{{$value->bu_id}}')">ลบ</button>
                                  </div>
                                </div></td>
                                    </tr>
                                    @endforeach
                                 
                                  </tbody></table>
                    </div>
                </div>
        
                          
                <div class="box-footer clearfix">
                    <div class="pull-left">Total : {{ $businessUnit->total() }}</div>
        
                          <ul class="pagination pagination-sm no-margin pull-right">
                          {{ $businessUnit->links() }}
                          </ul>
                        </div>
    </div><!-- /.box-body -->

  </div><!-- /.box -->

  <div class="modal fade" id="modelDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">เตือน</h4>
          </div>
          <div class="modal-body">
            Do you want to delete this business Unit?
          </div>
          <form id="deleteUserForm" method="POST" action="{{ url("manage/business-unit/delete") }}">
              {{-- {{ method_field("DELETE") }} --}}
              {{ csrf_field() }}
              <input type="hidden" name="iddelete" id="iddelete">
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            <button type="submit" class="btn btn-primary">OK</button>
            
          </div>
        </form>
        </div>
      </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> 
    function ShowEdit(name,id){

      let url = {!! json_encode(url("manage/business-unit")) !!}

      $('#EditModal').modal('show') 
      $('#bu_name_edit').val(name);
      $('#idedit').val(id);
     
    }
    </script>

<script> 
    function ShowDelete(id){
      let url = {!! json_encode(url("manage/business-unit/delete")) !!}
      console.log(id,url)
      $('#modelDelete').modal('show') 
      $('#iddelete').val(id)
    }
    </script>
@stop