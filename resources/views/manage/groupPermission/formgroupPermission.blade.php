@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Group Permission Management <small>ระบบจัดการ Group Permission</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Group Permission Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
         @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/permission/') }}" method="POST">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->id  }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">

                              <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Group User</label>
                     
                                    <div class="col-sm-10">
                                            <select class="form-control" name="group_user" id="group_user" @if(isset($edit)) disabled @endif>
                                                @foreach($GroupUser as $key=>$value)
                                                {{-- {{ dd($value->group_user_id) }} --}}
                                                    <option @if(isset($edit) && trim($edit[0]->group_user_id)==trim($value->group_user_id)) selected @else value="{{ $value->group_user_id }}" @endif  >{{ $value->group_user_name }}</option>
                                                @endforeach
                                                
                                                  </select>
                                          </div>
    
                                   
                                  </div>

                                  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Module</label>
                      
                                        <div class="col-sm-10">
                                                <select class="form-control" name="module" id="module" @if(isset($edit)) disabled @endif>
                                                        @if(isset($edit))
                                                    @foreach($Module as $key=>$value)
                                                        <option @if(isset($edit) && $edit[0]->module_id==$value->module_id) selected  @endif value="{{ $value->module_id }}">{{ $value->module_title }}</option>
                                                    @endforeach
                                                    @endif
                                                      </select>
                                                      
                                              </div>
        
                                       
                                      </div>

                                  <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Permission</label>
                      
                                        <div class="col-sm-10">
                          
                                                <label class="checkbox-inline"><input type="checkbox" name="is_add" value="1" @if(isset($edit) && $accessData->is_add=="1" ) checked  @endif>Add</label>
                                                <label class="checkbox-inline"><input type="checkbox" name="is_view" value="1" @if(isset($edit) && $accessData->is_view=="1") checked  @endif>View</label>
                                                <label class="checkbox-inline"><input type="checkbox" name="is_edit" value="1" @if(isset($edit) && $accessData->is_edit=="1") checked  @endif>Edit</label>
                                                <label class="checkbox-inline"><input type="checkbox" name="is_remove" value="1" @if(isset($edit) && $accessData->is_remove=="1") checked  @endif>Remove</label>
                                           
                                        </div>
                                      </div>

                 

                              

{{-- 
                                          <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="businessunit">
                                                            @foreach($businessUnit as $key=>$value)
                                                                <option value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                            @endforeach
                                                            
                                                              </select>
                                                      </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Product</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="template_product">
                                                                @foreach($templateProduct as $key=>$value)
                                                                    <option value="{{ $value->tmppd_id }}">{{ $value->tmppd_name }}</option>
                                                                @endforeach
                                                                
                                                                  </select>
                                                      </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Redeem</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control"  name="template_redeem">
                                                                @foreach($templateRedeem as $key=>$value)
                                                                    <option value="{{ $value->tmprd_id }}">{{ $value->tmprd_name }}</option>
                                                                @endforeach
                                                                
                                                                  </select>
                                                      </div>
                
                                               
                                              </div> --}}



            
                                          

                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/permission') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>

    <script>

        var edit = {!! isset($edit)?"true":"false" !!};

        console.log((edit))

        if(!edit){
            $.ajax({
url: "{{ url('manage/permission/getModule') }}",
method: "POST",
data: {
    query: $('#group_user').val(),
    _token: $('meta[name="csrf-token"]').attr('content'),

},
success: function (data) {
    // console.log(data)
    var htmldata = $(data);

     if(data===""){
        $('#module').attr('disabled',true);

    }else{
        // $('#module').attr('disabled',false);
        $('#module').append(data);
    }
    
}
});
        }   

      



        
    $('#group_user').on('change', function () {

$('#module').empty();


$.ajax({
url: "{{ url('manage/permission/getModule') }}",
method: "POST",
data: {
    query: $('#group_user').val(),
    _token: $('meta[name="csrf-token"]').attr('content'),

},
success: function (data) {
    // console.log(data)
    var htmldata = $(data);

     if(data===""){
        $('#module').attr('disabled',true);

    }else{
        $('#module').attr('disabled',false);
        $('#module').append(data);
    }
    
}
});
});



$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop