@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Redeem Template Product Management <small>ระบบจัดการ Redeem Template Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Redeem Template Product Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/redeem-template/') }}" method="POST" autocomplete="off">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->tmprd_id  }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">
                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Template Name</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input required type="text" class="form-control" id="t_name" name="t_name"  @if(isset($edit)) value="{{ $edit[0]->tmprd_name }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                </div>
                                </div>
                              </div>


                                  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                      
                                        <div class="col-sm-10">
                                                <select class="form-control" name="businessunit" id="businessunit" @if(isset($edit) ) disabled @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                        @foreach($businessUnit as $key=>$value)
                                                            <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                        @endforeach
                                                        
                                                          </select>
                                              </div>
        
                                      </div>

                                      <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Redeem</label>
                      
                                        <div class="col-sm-6">
                                                <select name="redeem" class="form-control"  id="redeem"></select>
                                          {{-- <input type="text" class="form-control" id="redeem" name="redeem"  @if(isset($edit)) value="{{ $edit[0]->gp_name }}" @endif> --}}
                                          {{-- <div id="groupProductList" style="position: absolute"> --}}
                                          {{-- </div> --}}
                                        </div>

                                        <div class="col-sm-2">
                                            <i class="fa fa-plus" style="font-size:30px"></i>
                                        </div>
                                      </div>

                                      <div class="row" style="margin-top:20px">
                            
                                        <div class="col-sm-12">

                                            <table id="groupTable" class="table table-bordered table-hover sorted_table">
                                                    <thead>
                                                <tr>
                                                    <th> Sequence   </th>
                                                    <th> Code   </th>
                                                    <th> Redeem Name   </th>
                                                    <th> Action   </th>
                                                </tr>
                                                    </thead>

                                                <tbody >
                                                    </tbody>

                                            </table>
                                        </div>
                

                                      </div>
        

                 <input type="hidden" name="tableredeem" id="tableredeem">

                              
                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/redeem-template') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
  
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')



    <script>      

     


    $(document).ready(function () {


        removegroup = function(index){
            dataTable.splice(index,1)
            $('#groupTable tbody').empty();

            $('#redeem').empty();

           

            
$.ajax({
    url: "{{ url('manage/redeem-template/getRedeem') }}",
    method: "POST",
    data: {
        query: $('#businessunit').val(),
        data: dataTable,
        _token: $('meta[name="csrf-token"]').attr('content'),

    },
    success: function (data) {
        console.log(data)
        var htmldata = $(data);


        $('#redeem').append(data);
        if($('#redeem').val()!==null){
                $('#redeem').prop('disabled', false);
            }
        
    }
});


        

         
         dataTable.map((value,index)=>
    $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.redeem_code}</td><td>${value.redeem_desc}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )

            $('#tableredeem').val(JSON.stringify(dataTable))

            if(dataTable.length===0){
                $('#tableredeem').val("")
                console.log("hello")
            }

        }


  var dataTable = [];

var edit = {!! isset($edit)?json_encode($MP_Redeem_Template):"null" !!};


if(edit!==null){
//    console.log(edit)

var Redeem = {!! isset($Redeem)?json_encode($Redeem):"null" !!}


    edit.map((value,index)=>{

        Redeem.map((data,index)=>{
            if(data.rd_id==value.rd_id){
                dataTable.push({id: value.rd_id,
                redeem_code:data.rd_code,
                redeem_desc: data.rd_desc})
            }
        }
        )
        
    })

    
    dataTable.map((value,index)=>
    $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.redeem_code}</td><td>${value.redeem_desc}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )
            $('#tableredeem').val(JSON.stringify(dataTable))
}

        var idAddRD;
        var rdcode;

        var query = $('#businessunit').val();

console.log(query)


$.ajax({
    url: "{{ url('manage/redeem-template/getRedeem') }}",
    method: "POST",
    data: {
        query: query,
        data: dataTable,
        _token: $('meta[name="csrf-token"]').attr('content'),

    },
    success: function (data) {

        console.log('====================================');
        console.log(data);
        console.log('====================================');

        var htmldata = $(data);
        
              htmldata.each(function(){
                dataTable.map((val)=>{
                      if(val.redeem_code===$(this).attr('name')){
                        $(this).remove()
                    }
                })
                console.log($(this).attr('name'))
            })

        $('#redeem').append(htmldata);
        if($('#redeem').val()===null){
                $('#redeem').prop('disabled', 'disabled');
            }
    }
});


$('#businessunit').on('change', function () {

     $('#redeem').empty()

    $('#groupTable tbody').empty();

    dataTable = [];

$('#tableredeem tbody').empty();


$('#tableredeem').val(JSON.stringify(dataTable))

    $.ajax({
    url: "{{ url('manage/redeem-template/getRedeem') }}",
    method: "POST",
    data: {
        query: this.value,
        data: dataTable,
        _token: $('meta[name="csrf-token"]').attr('content'),

    },
    success: function (data) {
        console.log(data)
        var htmldata = $(data);


        $('#redeem').append(data);
    }
});
});

              $('tbody').sortable({
    start: function(event, ui) {
        ui.item.startPos = ui.item.index();
    },
    stop: function(event, ui) {
        console.log("Start position: " + ui.item.startPos);
        console.log("New position: " + ui.item.index());

        var temp = dataTable[ui.item.startPos];

         dataTable[ui.item.startPos] = dataTable[ui.item.index()]
        dataTable[ui.item.index()] = temp;

          $('#groupTable tbody').empty();

         
         dataTable.map((value,index)=>
    $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.redeem_code}</td><td>${value.redeem_desc}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )

            $('#tableredeem').val(JSON.stringify(dataTable))
       

    }
});

       

      
  
        

     

        $(document).on('click', 'i', function () {

            

        
            if($('#redeem').val()===null){
            alert("Select Group Product");
        }else{
            dataTable.push({
                id: $('#redeem').val(),
                redeem_desc: $("#redeem option:selected").text(),
                redeem_code: $("#redeem option:selected").attr('name')
            })
        }
    

            $("#redeem option:selected").remove();

            if($('#redeem').val()===null){
                $('#redeem').prop('disabled', 'disabled');
            }

            $('#groupTable tbody').empty();
            // $('#redeem').val("")

         
            dataTable.map((value,index)=>
            $('#groupTable > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.redeem_code}</td><td>${value.redeem_desc}</td><td> <button type="button" style="width:60px" onclick="removegroup(${index})" class="btn btn-block btn-danger btn-flat">ลบ</button></td></tr>`)
            )
            $('#tableredeem').val(JSON.stringify(dataTable))
            
        })


        $(document).on('click', 'li', function () {
            $('#redeem').val($(this).text());
            idAddRD = $(this).attr('id')
            rdcode =  $(this).attr('name')
// {{-- 
//             console.log('====================================');
//             console.log(rdcode);
//             console.log('===================================='); --}}

            $('#groupProductList').fadeOut();
        });

    }); 
    
    
    </script>

    <script>







$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop