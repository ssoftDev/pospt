@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Group Product Management <small>ระบบจัดการ Group Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Group Product Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if (flashMe()->ok())
        {!! flashMe_flash() !!}
      @endif
        <form id="deleteUserForm" action={{ url('manage/group-product/') }} method="get">
          <div class="row">
              <div class="col-sm-4">
                <div class="input-group">
                  <input type="text" value="{!! \Request::get('q') !!}" name="q" class="form-control">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>
  
              <div class="col-sm-4">
                  <button type="submit"  style="width:70px" class="btn btn-block btn-success btn-flat" >ค้นหา</button>
          </div>
            </div>
  
          
        </form>

            <div class="row" style="margin-top:20px">
                    <div class="col-sm-4">
                            <a href="{{ url('manage/group-product/getform') }}"  style="width:70px" class="btn btn-block btn-success btn-flat" >เพิ่ม</a>
                    </div>
                </div>
                           
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-12">
                            <table class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 10px">No</th>
                                      <th>Group Name</th>
                                      <th>Group Product Template</th>
                                      <th>Business Unit</th>
                                      <th>Enable</th>
                                      
                                      <th>Created Date</th>
                                      <th>Created By</th>
                                      <th>Updated Date</th>
                                      <th>Updated By</th>
                                      <th>View Product</th>
                                      <th width="150px">Action</th>
 
                                    </tr>
                                    @foreach($groupProduct as $index=>$value)
                                    <tr>
                                        <td>{{ (($index+1)+($groupProduct->currentPage()*$groupProduct->perPage()))-10 }}</td>
                                      <td>{{ $value->gp_name }}</td>
                                      <td>{{ $value->GroupProductTemplate['gt_name'] }}</td>
                                      <td>{{ $value->BusinessUnit['bu_name'] }}</td>
                                      <td>{!! $value->is_enable=='y'?'Public':'Private' !!}</td>
                                      
                                      <td>{{ $value->create_date }}</td>
                                      <td>{{ $value->create_by }}</td>
                                      <td>{{ $value->update_date }}</td>
                                      <td>{{ $value->update_by }}</td>
                                      <td><button type="button" style="width:60px" onclick="onViewProduct('{{ $value->gp_id }}','{{ $value->gp_name }}')" class='btn btn-block btn-primary btn-flat'>view</button></td>
                                      <td><div class="row" >
                                          <div class="col-sm-5">
                                          <a href="{{ url('manage/group-product/edit').'/'.$value->gp_id }}" style="width:60px" class="btn btn-block btn-warning btn-flat">แก้ไข</a>
                                          </div>
                                          <div class="col-sm-5">
                                              <button type="button" style="width:60px" class="btn btn-block btn-danger btn-flat" onclick="ShowDelete('{{$value->gp_id}}')">ลบ</button>
                                          </div>
                                        </div></td>
        
                                    </tr>
                                    @endforeach
                                 
                                  </tbody></table>
                    </div>
                </div>
        
                          
                             
                <div class="box-footer clearfix">
                    <div class="pull-left">Total : {{ $groupProduct->total() }}</div>
  
                          <ul class="pagination pagination-sm no-margin pull-right">
                          {{ $groupProduct->links() }}
                          </ul>
                        </div>
    </div><!-- /.box-body -->

  </div><!-- /.box -->

  <div class="modal fade" id="modelDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">เตือน</h4>
            </div>
            <div class="modal-body">
              Do you want to delete this Group Product?
            </div>
            <form id="deleteUserForm" method="POST" action="{{ url("manage/group-product/delete") }}">
                {{ csrf_field() }}
                <input type="hidden" name="iddelete" id="iddelete">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
              <button type="submit" class="btn btn-primary">OK</button>
              
            </div>
          </form>
          </div>
        </div>
      </div>

      <div class="modal fade bs-example-modal-lg" id="modalViewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">View Product</h4>
              </div>
              <div class="modal-body" style="max-height: calc(100vh - 200px);
              overflow-y: auto;">
                      <table id="tableViewProduct" class="table table-bordered table-hover">
                              <thead>
                          <tr>
                              <th> No   </th>
                              <th> Product Name   </th>
                              <th> Price   </th>
                              <th> Remark </th>
                              <th width="20%"> Picture </th>
                          </tr>
                              </thead>
  
                          <tbody >
                              </tbody>
  
                      </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
  
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> 

function onViewProduct(index,namegp){
        $('#modalViewProduct').modal('show');

        $('#myModalLabel').text('View Product Group ' +namegp)

        $.ajax({
        url: "{{ url('manage/group-template-product/getProduct') }}",
        method: "POST",
        data: {
            query: index,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            $('#tableViewProduct tbody').empty();

            data.data.map((value,index)=>{
                $('#tableViewProduct > tbody:last-child').append(`<tr><td>${index+1}</td><td>${value.product.p_name}</td><td>${value.product.p_price}</td><td>${value.product.remark}</td><td><img src="${value.product.img_url}" style="width:100%;" alt="" srcset=""></td></tr>`)
            })
            console.log(data)
            // $('#gp_name').append(htmldata);
        }
    });
    }

        function ShowDelete(id){
          let url = {!! json_encode(url("manage/group-product/delete")) !!}
          console.log(id,url)
          $('#modelDelete').modal('show') 
      $('#iddelete').val(id)
        }
        </script>
@stop