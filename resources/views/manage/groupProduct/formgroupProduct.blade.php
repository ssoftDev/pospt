@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Group Product Management <small>ระบบจัดการ Group Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Group Product Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
         @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/group-product/') }}" method="POST">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->gp_id  }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">
                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Group Name</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input required type="text" class="form-control" id="g_name" name="g_name"  @if(isset($edit)) value="{{ $edit[0]->gp_name }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                        </div>
                                </div>
                              </div>

                              <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Product Tab</label>
                  
                                    <div class="col-sm-10">
                                            <select class="form-control" name="gt_product" id="gt_product">
                                                @foreach($groupTemplateProduct as $key=>$value)
                                                    <option @if(isset($edit) && $edit[0]->gt_id==$value->gt_id) selected  @endif  value="{{ $value->gt_id }}">{{ $value->gt_name }}</option>
                                                @endforeach
                                                
                                                  </select>
                                          </div>
    
                                   
                                  </div>

                                  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                      
                                        <div class="col-sm-10">
                                                <select class="form-control" name="businessunit" @if(isset($edit) ) disabled @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                    @foreach($businessUnit as $key=>$value)
                                                        <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                    @endforeach
                                                    
                                                      </select>
                                              </div>
        
                                       
                                      </div>

                                  <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Status</label>
                      
                                        <div class="col-sm-10">
                          
                                                <label class="radio-inline"><input type="radio" name="status" value="y" @if(isset($edit) && $edit[0]->is_enable=='y') checked @else checked  @endif>Public</label>
                                                <label class="radio-inline"><input type="radio" name="status" value="n" @if(isset($edit) && $edit[0]->is_enable=='n') checked  @endif>Private</label>

               
                                           
                                        </div>
                                      </div>

                 

                              

{{-- 
                                          <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="businessunit">
                                                            @foreach($businessUnit as $key=>$value)
                                                                <option value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                            @endforeach
                                                            
                                                              </select>
                                                      </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Product</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="template_product">
                                                                @foreach($templateProduct as $key=>$value)
                                                                    <option value="{{ $value->tmppd_id }}">{{ $value->tmppd_name }}</option>
                                                                @endforeach
                                                                
                                                                  </select>
                                                      </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Redeem</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control"  name="template_redeem">
                                                                @foreach($templateRedeem as $key=>$value)
                                                                    <option value="{{ $value->tmprd_id }}">{{ $value->tmprd_name }}</option>
                                                                @endforeach
                                                                
                                                                  </select>
                                                      </div>
                
                                               
                                              </div> --}}



            
                                          

                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/group-product') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log($('#gt_product').val()); </script>

    <script>

if($('#gt_product').val()==null){
    $('#gt_product').prop( "disabled", true );
}

$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop