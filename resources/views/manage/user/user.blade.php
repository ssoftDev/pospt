@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>User Management <small>ระบบจัดการ User</small></h1>
    
@stop

@section('content')
<div class="box">
    @if (flashMe()->ok())
    {!! flashMe_flash() !!}
  @endif
    <div class="box-header with-border">
      <h3 class="box-title">User Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">

        <form id="deleteUserForm" action={{ url('manage/users/') }} method="get">
        <div class="row">
            <div class="col-sm-4">
              <div class="input-group">
                <input type="text" value="{!! \Request::get('q') !!}" name="q" class="form-control">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <select style="margin-left:10px" value="{!! \Request::get('s') !!}" name="s" class="form-control">
                  <option value="U">User</option>
                  <option value="S">Store</option>
                </select>
              </div>
            </div>



            <div class="col-sm-4">
                <button type="submit"  style="width:70px" class="btn btn-block btn-success btn-flat" >ค้นหา</button>
        </div>
          </div>

        
      </form>
       

      <div class="row" style="margin-top:20px">
          <div class="col-sm-4">
                  <a href="{{ url('manage/users/getform') }}"  style="width:70px" class="btn btn-block btn-success btn-flat" >เพิ่ม</a>
          </div>
      </div>
                   
        <div class="row" style="margin-top: 20px">
            <div class="col-sm-12">
                    <table class="table table-bordered">
                            <tbody><tr>
                              <th style="width: 10px">No</th>
                              <th>User</th>
                              <th>Business Unit</th>
                              <th>Store</th>
                              <th>Created Date</th>
                              <th>Created By</th>
                              <th>Updated Date</th>
                              <th>Updated By</th>
                              <th>Reset Password</th>
                              <th width="150px">Action</th>
                            </tr>
                            @foreach($user as $index=>$value)
                            <tr>
                                <td>{{ (($index+1)+($user->currentPage()*$user->perPage()))-10 }}</td>
                              <td>{{ $value->user }}</td>
                              <td>{{ $value->BusinessUnit['bu_name'] }}</td>
                              <td>{{ $value->Store['store_name'] }}</td>
                              <td>{{ $value->create_date }}</td>
                              <td>{{ $value->create_by }}</td>
                              <td>{{ $value->update_date }}</td>
                              <td>{{ $value->update_by }}</td>
                              <td >
                                  <div class="row" >
                                    <div class="col-sm-5">
                                        <button type="button" style="width:60px" class="btn btn-block btn-info btn-flat" onclick="ShowReset('{{$value->user}}')">รีเซ็ท</button>
                                    </div>
                                  </div>
                                  
                                </td>
                              <td >
                                <div class="row" >
                                  <div class="col-sm-5">
                                  <a href="{{ url('manage/users/edit').'/'.$value->user }}" style="width:60px" class="btn btn-block btn-warning btn-flat">แก้ไข</a>
                                  </div>
                                  <div class="col-sm-5">
                                      <button type="button" style="width:60px" class="btn btn-block btn-danger btn-flat" onclick="ShowDelete('{{$value->user}}')">ลบ</button>
                                  </div>
                                </div>
                                
                                
                              </td>

                            </tr>
                            @endforeach
                         
                          </tbody></table>
            </div>
        </div>

                  
        <div class="box-footer clearfix">
            <div class="pull-left">Total : {{ $user->total() }}</div>

                  <ul class="pagination pagination-sm no-margin pull-right">
                  {{ $user->links() }}
                  </ul>
                </div>
            
    </div><!-- /.box-body -->

    <div class="modal fade" id="modelDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">เตือน</h4>
            </div>
            <div class="modal-body">
              Do you want to delete this users?
            </div>
            <form id="deleteUserForm" method="POST" action="{{ url("manage/users/delete") }}">
                {{ csrf_field() }}
                <input type="hidden" name="iddelete" id="iddelete">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
              <button type="submit" class="btn btn-primary">OK</button>
              
            </div>
          </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modelReset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">เตือน</h4>
              </div>
              <div class="modal-body">
                Do you want to reset Password?
              </div>
              <form id="deleteUserForm" method="POST" action="{{ url("manage/users/resetpassword") }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="idreset" id="idreset">
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                <button type="submit" class="btn btn-primary">OK</button>
                
              </div>
            </form>
            </div>
          </div>
        </div>
  
  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> 
    function ShowDelete(id){
      let url = {!! json_encode(url("manage/users/delete")) !!}
      console.log(id,url)
      $('#modelDelete').modal('show') 
      $('#iddelete').val(id)
    }

    function ShowReset(id){
      let url = {!! json_encode(url("manage/users/resetpassword")) !!}
      console.log(id,url)
      $('#modelReset').modal('show') 
      $('#idreset').val(id)
    }
    </script>

    
@stop