@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

    <h1>User Management <small>ระบบจัดการ User</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">User Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/users/') }}" method="POST" autocomplete="off">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        
                        <input type="hidden" name="id" value="{{ $edit[0]->user  }}">
                        @endif
                        {{csrf_field()}}
                            <div class="box-body">

                                    <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Group User</label>
                          {{-- {{dd(old("group_user") )}} --}}
                                            <div class="col-sm-10">
                                                    <select class="form-control" name="group_user"  value="{{ old("group_user") }}" id="group_user" >
                                                        @foreach($GroupUser as $key=>$value)
                                                            <option @if(isset($edit) && $edit[0]->group_user_id==$value->group_user_id) selected @elseif(old('group_user')==$value->group_user_id) selected @endif  value="{{ $value->group_user_id }}">{{ $value->group_user_name }}</option>
                                                        @endforeach
                                                        
                                                          </select>
                                                  </div>
            
                                           
                                          </div>

                                          <div class="form-group" id="formbu">
                                                <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                              
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="width:100%">
                                                        <select class="form-control" name="businessunit" id="businessunit" value="{{ old("businessunit") }}" @if(isset($edit)) readonly @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                            @foreach($businessUnit as $key=>$value)
                                                                <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @elseif(old('businessunit')==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                            @endforeach
                                                            
                                                              </select>
                                                              <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                                      </div>
                                                </div>
                                               
                                              </div>

                                              <div class="form-group" id="formstore" hidden="true">
                                                    <label for="inputPassword3" class="col-sm-2 control-label">Store</label>
                                  
                                                    <div class="col-sm-10">
                                                        <div class="input-group">
                                                           <select class="js-example-basic-single" name="store" id="store" value="{{ old("businessunit") }}" @if(isset($edit)) readonly @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                            {{-- @foreach($businessUnit as $key=>$value)
                                                                <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected   @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                            @endforeach --}}

                                                            <?php
                                                             $store = DB::table('store')->get();
                                                                foreach ($store as $key => $value) {
                                                                        if(isset($edit) && $edit[0]->store_id==$value->store_id){
                                                                         echo("<option selected value='{$value->store_id}'>{$value->store_name}</option>");
                                                                    }else{
                                                                        if(old("store")==$value->store_id){
                                                                            echo("<option selected value='{$value->store_id}'>{$value->store_name}</option>");
                                                                        }else{
                                                                            echo("<option value='{$value->store_id}'>{$value->store_name}</option>");
                                                                        }
                                                                    
                                                                    }
                                                                }
                                                            ?>
                                                            
                                                              </select>
                                                                <span class="input-group-addon" ><i class="fa fa-search"></i></span>
                                                                <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                                        </div>
                                                    </div>
                                                   
                                                  </div>
        
                                               


                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">User</label>
              
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="user" name="user" @if(isset($edit)) disabled @endif @if(!isset($edit))   @endif value="@if(isset($edit)){{ $edit[0]->user }} @endif">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
              
                                <div class="col-sm-10">
                                  <input type="password" class="form-control" id="pass" name="pass" readonly value="@if(isset($edit)){{ $edit[0]->pass }}@else{{ $password }}@endif">
                                </div>

                               
                              </div>

                              <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
                  
                                    <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                            <input type="email" name="email" class="form-control" @if(isset($edit)) value="{{ $edit[0]->email }}" @else value="{{ old('email') }}"  @endif   required>
                                             <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                        </div>
                                            </div>
    
                                   
                                  </div>

                                  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Tel</label>
                      
                                        <div class="col-sm-10">
                                                <input onkeypress='validate(event)' type="tel" class="form-control" id="email" @if(isset($edit)) value="{{ $edit[0]->tel }}" @else value="{{ old('tel') }}" @endif maxlength="12" name="tel" >
                                              </div>
        
                                       
                                      </div>

                                   
            

                                          <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Send User</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="send">
                                                                <option value="N" @if(isset($edit) && $edit[0]->send=='N') selected @endif>None</option>
                                                                <option value="EMAIL" @if(isset($edit) && $edit[0]->send=='EMAIL') selected @endif>Email</option>
                                                                <option value="SMS">Phone</option>
                                                              </select>
                                                      </div>
                
                                               
                                              </div>

            
                                          

                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/users') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" id="savebutton" disabled class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>


    

    <script>

        $(document).ready(function() {
    $('.js-example-basic-single').select2({ 
    width: '100%'});
});

        var user = {!! isset($user)?json_encode($user):json_encode($edit[0]->user) !!};
        var edit = {!! isset($edit[0])?true:false !!}

        console.log(edit)

 if($('#group_user').val()==2){
                $('#formstore').hide();
                $('#formbu').show();
                $('#savebutton').attr('disabled',false)
            }else if($('#group_user').val()==3){
                
                if($('#store')!==""){
                    $('#savebutton').attr('disabled',false)
                }
                $('#formbu').hide();
                $('#formstore').show();
                $('#user').val($('#store').val())
            }else{
                    $('#formbu').hide();
                $('#formstore').hide();
                $('#user').val(user);
            }

             $('#store').on('change', function() {
                  $('#user').val(this.value)
             });

        $('#group_user').on('change', function() {
            console.log(this.value)
            if(this.value==2){
                $('#formstore').hide();
                $('#formbu').show();
                $('#savebutton').attr('disabled',false)
                if(edit==undefined)
                 $('#user').val("")
            }else if(this.value==3){
                
                if($('#store')!==""){
                    $('#savebutton').attr('disabled',false)
                }
                $('#formbu').hide();
                $('#formstore').show();
                if(edit==undefined)
                $('#user').val($('#store').val())
            }else if(this.value==1){
                    $('#formbu').hide();
                $('#formstore').hide();
                if(edit==undefined)
                $('#user').val(user);
            }
});

        function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
            $(document).ready(function(){

                if($('#store')!==""){
                    $('#savebutton').attr('disabled',false)
                }
            
             $('#store').keyup(function(){ 
                $('#savebutton').attr('disabled',true)
                    var query = $(this).val();
                    if(query != '')
                    {
    
                     $.ajax({
                      url:"{{ url('manage/users/getstore') }}",
                      method:"POST",
                      data:{query:query, _token: $('meta[name="csrf-token"]').attr('content')},
                      success:function(data){
                          console.log(data)
                       $('#storeList').fadeIn();  
                                $('#storeList').html(data);
                      }
                     });
                    }
                });

                $('#store').on('focus',function(){ 
                $('#savebutton').attr('disabled',true)
                    var query = $(this).val();
                    if(query != '')
                    {
    
                     $.ajax({
                      url:"{{ url('manage/users/getstore') }}",
                      method:"POST",
                      data:{query:query, _token: $('meta[name="csrf-token"]').attr('content')},
                      success:function(data){
                          console.log(data)
                       $('#storeList').fadeIn();  
                                $('#storeList').html(data);
                      }
                     });
                    }
                });
            
                $(document).on('click', 'li', function(){  
                    $('#store').val($(this).text());  
                    $('#storeList').fadeOut();  
                    $('#savebutton').attr('disabled',false)
                });  
            
            });
            </script>
            
@stop