@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Store Management <small>ระบบจัดการ Store</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Store Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/store/') }}" method="POST">
                        {{ csrf_field() }}
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->store_id  }}">
                        @endif
                            <div class="box-body">
                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Store Name</label>
              
                                <div class="col-sm-10">
                                    <div class="input-group" style="width:100%">
                                  <input type="text" class="form-control" id="store_name" name="store_name" required @if(isset($edit)) value="{{ $edit[0]->store_name }}" @else value="{{ old("store_name") }}"  @endif>
                                 <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                    </div>
                                </div>
                              </div>

                               <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Address</label>
              
                                <div class="col-sm-10">
                                    <div class="input-group" style="width:100%">
                                  <input type="text" class="form-control" id="address" name="address" required @if(isset($edit)) value="{{ $edit[0]->address }}" @else value="{{ old("address") }}"  @endif>
                                 <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                    </div>
                                </div>
                              </div>

                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Tel</label>
              
                                <div class="col-sm-10">
                                    <div class="input-group" style="width:100%">
                                  <input type="text" class="form-control" id="tel" name="tel" required @if(isset($edit)) value="{{ $edit[0]->tel }}" @else value="{{ old("tel") }}"  @endif>
                                 <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                    </div>
                                </div>
                              </div>

                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Email</label>
              
                                <div class="col-sm-10">
                                    <div class="input-group" style="width:100%">
                                  <input type="email" class="form-control" id="email" name="email" required @if(isset($edit)) value="{{ $edit[0]->email }}" @else value="{{ old("email") }}"  @endif>
                                 <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                    </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">TID</label>
              
                                
                                <div class="col-sm-10">
                                     <div class="input-group" style="width:100%">
                                            <input type="text" class="form-control" id="tid" name="tid" required @if(isset($edit)) value="{{ $edit[0]->tid }}" @else value="{{ old("tid") }}" @endif>
                                            <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                    </div>
                                </div>

                               
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">MID</label>
              
                                <div class="col-sm-10">
                                    <div class="input-group" style="width:100%">
                                  <input type="text" required class="form-control" id="mid" name="mid" @if(isset($edit)) value="{{ $edit[0]->mid }}" @else value="{{ old("mid") }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                </div>
                                </div>

                               
                              </div>


                                          <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                              
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="width:100%">
                                                            <select class="form-control" id="businessunit" name="businessunit" @if(isset($edit) ) readonly @elseif(trim(\Auth::user()->group_user_id) != "1") readonly @endif>
                                                                    @foreach($businessUnit as $key=>$value)
                                                                        <option @if(isset($edit) && $edit[0]->bu_id==$value->bu_id) selected  @endif value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                                    @endforeach
                                                                    
                                                                      </select>
                                                              <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                                      </div>
                                                </div>
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Product</label>
                              
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="width:100%">
                                                        <select class="form-control" name="template_product" id="templateProduct"  value="{{ old("template_product") }}" required>
                                                                {{-- @foreach($templateProduct as $key=>$value)
                                                                    <option @if(isset($edit) && $edit[0]->tmppd_id==$value->tmppd_id) selected  @endif value="{{ $value->tmppd_id }}">{{ $value->tmppd_name }}</option>
                                                                @endforeach --}}
                                                                
                                                                  </select>
                                                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                                      </div>
                                                </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Redeem</label>
                              
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="width:100%">
                                                        <select class="form-control"  name="template_redeem" id="template_redeem" value="{{ old("template_redeem") }}" required>
                                                                {{-- @foreach($templateRedeem as $key=>$value)
                                                                    <option @if(isset($edit) && $edit[0]->tmprd==$value->tmprd_id) selected  @endif value="{{ $value->tmprd_id }}">{{ $value->tmprd_name }}</option>
                                                                @endforeach --}}
                                                                
                                                                  </select>
                                                                   <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>

                                                      </div>
                
                                               
                                              </div>



            
                                          

                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/store') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" id="savebutton" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>

    <script>

         $("#tid").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

     $("#mid").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

         var query = $('#businessunit').val();

         var edit = {!! isset($edit)?json_encode($edit[0]):null !!}

         console.log('====================================');
         console.log(query);
         console.log('====================================');

         $.ajax({
        url: "{{ url('manage/store/getTemplateProduct') }}",
        method: "POST",
        data: {
            query: query,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            console.log(data)
            var htmldata = $(data);
            if(data===""){
                $('#templateProduct').attr('disabled',true);
            }else{
                $('#templateProduct').attr('disabled',false);
                $('#templateProduct').append(data);

                if(edit!==null){
                    $("#templateProduct option[value='"+ edit.tmppd_id +"']").attr('selected','selected');
                }
            }
            

            // $('#templateProduct').append(data);
        }
    });

     $.ajax({
        url: "{{ url('manage/store/getTemplateRedeem') }}",
        method: "POST",
        data: {
            query: query,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            console.log(data)
            var htmldata = $(data);

   if(data===""){
                $('#template_redeem').attr('disabled',true);
            }else{
                $('#template_redeem').attr('disabled',false);
                $('#template_redeem').append(data);
            }
            if(edit!==null){
                    $("#template_redeem option[value='"+ edit.tmprd +"']").attr('selected','selected');
                }
        }
    });

    $('#businessunit').on('change', function () {

        $('#template_redeem').empty();

         $('#templateProduct').empty();
        
        $.ajax({
        url: "{{ url('manage/store/getTemplateProduct') }}",
        method: "POST",
        data: {
            query: this.value,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            console.log(data)
            var htmldata = $(data);

             if(data===""){
                $('#templateProduct').attr('disabled',true);
            }else{
                $('#templateProduct').attr('disabled',false);
                $('#templateProduct').append(data);
            }
            
        }
    });

     $.ajax({
        url: "{{ url('manage/store/getTemplateRedeem') }}",
        method: "POST",
        data: {
            query: this.value,
            _token: $('meta[name="csrf-token"]').attr('content'),

        },
        success: function (data) {
            console.log(data)
            var htmldata = $(data);

            if(data===""){
                $('#template_redeem').attr('disabled',true);

            }else{
                $('#template_redeem').attr('disabled',false);
                $('#template_redeem').append(data);
            }
        }
    });


    });

        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
            $(document).ready(function(){

                
            
             $('#store').keyup(function(){ 
                    var query = $(this).val();
                    if(query != '')
                    {
    
                     $.ajax({
                      url:"{{ url('manage/users/getstore') }}",
                      method:"POST",
                      data:{query:query, _token: $('meta[name="csrf-token"]').attr('content')},
                      success:function(data){
                          console.log(data)
                       $('#storeList').fadeIn();  
                                $('#storeList').html(data);
                      }
                     });
                    }
                });
            
                $(document).on('click', 'li', function(){  
                    $('#store').val($(this).text());  
                    $('#storeList').fadeOut();  
                });  
            
            });
            </script>
            
@stop