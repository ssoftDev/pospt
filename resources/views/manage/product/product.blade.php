@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Product Management <small>ระบบจัดการ Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Product Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        @if (flashMe()->ok())
        {!! flashMe_flash() !!}
      @endif
        <form id="deleteUserForm" action={{ url('manage/product/') }} method="get">
          <div class="row">

                <div class="col-sm-4">
             <div class="input-group">
                <span class="input-group-addon"><b>Group Product</b></span>
                  <select name="gp" value="{!! \Request::get('gp') !!}" class="form-control" id="">
                  @if(trim(\Auth::user()->group_user_id) != "1")  <option value="all">All</option> @endif
                <?php

                

                                                                foreach ($groupProduct as $key => $value) {
                                                                        if(\Request::get('gp')==$value->gp_id){
                                                                         echo("<option selected value='{$value->gp_id}'>{$value->gp_name}</option>");
                                                                    }else{
                                                                    echo("<option value='{$value->gp_id}'>{$value->gp_name}</option>");
                                                                    }
                                                                }
                                                            ?>
                                                            </select>
                                                   
              </div>
              </div>
                <div class="col-sm-2">
             <div class="input-group">
                <span class="input-group-addon"><b>Type</b></span>
            
                  <select name="type" value="{!! \Request::get('type') !!}" class="form-control" id="">
                    <option value="all">All</option>
                <?php
                                                             $store = DB::table('type_product')->get();
                                                                foreach ($store as $key => $value) {
                                                                        if(\Request::get('type')==$value->type_product_id){
                                                                         echo("<option selected value='{$value->type_product_id}'>{$value->type_product_name}</option>");
                                                                    }else{
                                                                    echo("<option value='{$value->type_product_id}'>{$value->type_product_name}</option>");
                                                                    }
                                                                }
                                                            ?>
                                                            </select>
                                                       
              </div>
              </div>

              <div class="col-sm-3">
                <div class="input-group">
                  <input type="text" value="{!! \Request::get('q') !!}" name="q" class="form-control">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>


            
  
              <div class="col-sm-2">
                  <button type="submit"  style="width:70px" class="btn btn-block btn-success btn-flat" >ค้นหา</button>
          </div>
            </div>
  
          
        </form>

            <div class="row" style="margin-top:20px">
                    <div class="col-sm-4">
                            <a href="{{ url('manage/product/getform') }}"  style="width:70px" class="btn btn-block btn-success btn-flat" >เพิ่ม</a>
                    </div>
                </div>
                           
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-12">
                            <table class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 10px">No</th>
                                      <th>Product Name</th>
                                      <th>Price</th>
                                      <th>Remark</th>
                                      <th>Type</th>
                                      <th>Group Product</th>
                                      <th>Created Date</th>
                                      <th>Created By</th>
                                      <th>Updated Date</th>
                                      <th>Updated By</th>
                                      <th width="150px">Action</th>
 
                                    </tr>
                                    @foreach($product as $index=>$value)
                                    <tr>
                                      <td>{{ (($index+1)+($product->currentPage()*$product->perPage()))-10 }}</td>
                                      <td>{{ $value->p_name }}</td>
                                      <td>{{ number_format($value->p_price,2) }}</td>
                                      <td>{{ $value->remark }}</td>
                                      <td>{{ $value->type_product_name }}</td>
                                      <td>{{ $value->gp_name }}</td>
                                      <td>{{ $value->create_date }}</td>
                                      <td>{{ $value->create_by }}</td>
                                      <td>{{ $value->update_date }}</td>
                                      <td>{{ $value->update_by }}</td>
                                      <td><div class="row" >
                                          <div class="col-sm-5">
                                          <a href="{{ url('manage/product/edit').'/'.$value->pid }}" style="width:60px" class="btn btn-block btn-warning btn-flat">แก้ไข</a>
                                          </div>
                                          <div class="col-sm-5">
                                              <button type="button" style="width:60px" class="btn btn-block btn-danger btn-flat" onclick="ShowDelete('{{$value->pid}}')">ลบ</button>
                                          </div>
                                        </div></td>
        
                                    </tr>
                                    @endforeach
                                 
                                  </tbody></table>
                    </div>
                </div>
        
                          
                <div class="box-footer clearfix">
                  <div class="pull-left">Total : {{ $product->total() }}</div>

                        <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $product->links() }}
                        </ul>
                      </div>
    </div><!-- /.box-body -->

  </div><!-- /.box -->


  <div class="modal fade" id="modelDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">เตือน</h4>
            </div>
            <div class="modal-body">
              Do you want to delete this Product?
            </div>
            <form id="deleteUserForm" method="POST" action="{{ url("manage/product/delete") }}">
                {{ csrf_field() }}
                <input type="hidden" name="iddelete" id="iddelete">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
              <button type="submit" class="btn btn-primary">OK</button>
              
            </div>
          </form>
          </div>
        </div>
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> 
        function ShowDelete(id){
          let url = {!! json_encode(url("manage/product/delete")) !!}
          console.log(id,url)
          $('#modelDelete').modal('show') 
      $('#iddelete').val(id)
        }
        </script>
@stop