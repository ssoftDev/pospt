@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<h1>Product Management <small>ระบบจัดการ Product</small></h1>
    
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Product Management</h3>
       
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
                <div class="col-sm-2"></div>
            <div class="col-sm-8">
                    <form class="form-horizontal" action="{{ url('manage/product/') }}" enctype="multipart/form-data" method="POST">
                        @if(isset($edit))  {{ method_field('PUT') }} 
                        <input type="hidden" name="id" value="{{ $edit[0]->pid  }}">
                        @endif
                        {{ csrf_field() }}
                            <div class="box-body">
                              <div class="form-group">
                                  
                                <label for="inputEmail3" class="col-sm-2 control-label pull-left">Product Name</label>
              
                                <div class="col-sm-10">
                                        <div class="input-group" style="width:100%">
                                  <input type="text" class="form-control" required id="p_name" name="p_name" @if(isset($edit)) value="{{ $edit[0]->p_name }}" @endif>
                                  <span style="color:red;position:absolute;margin-left:10px;font-size: 20px;"> *</span>
                                        </div>
                                </div>
                              </div>

                              <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Product Type</label>
                  
                                    <div class="col-sm-10">
                                            <select class="form-control" name="p_type" required>
                                                @foreach($typeProduct as $key=>$value)
                                                    <option @if(isset($edit) && $edit[0]->type_product_id==$value->type_product_id) selected  @endif value="{{ $value->type_product_id }}">{{ $value->type_product_name }}</option>
                                                @endforeach
                                                
                                                  </select>
                                          </div>
    
                                   
                                  </div>

                              <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label" required>Group</label>
                                        
                                    <div class="col-sm-10">
                                            <select class="form-control" name="p_group">
                                                @foreach($groupProduct as $key=>$value)
                                                    <option @if(isset($edit) && $mpProduct->gp_id==$value->gp_id) selected  @endif value="{{ $value->gp_id }}">{{ $value->gp_name }}</option>
                                                @endforeach
                                                
                                                  </select>
                                          </div>
    
                                   
                                  </div>

                                  <div class="form-group">
                                  
                                        <label for="inputEmail3" class="col-sm-2 control-label pull-left">Price</label>
                                        
                                        <div class="col-sm-10">
                                          <input required type="number" class="form-control" style="text-align: right"  min="0"  step="any" id="store_name"  name="price" @if(isset($edit)) value="{{ $edit[0]->p_price }}" @else value=0.00 @endif>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                  
                                            <label for="inputEmail3" class="col-sm-2 control-label pull-left">Image</label>
                          
                                            <div class="col-sm-10">
                                                    <input type="file" name="productpic" id="imgInp" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                                                    <img id="blah" src="#" alt="your image" style="max-width: 200px;margin-top: 20px"/>

                                                    
    
                                                    
                                                    
                                                    

                                            </div>
                                          </div>
                                          @if(isset($edit) && $edit[0]->img_url) 
                                          <div class="form-group">
                                  
                                            <label for="inputEmail3" class="col-sm-2 control-label pull-left">Now Image</label>
                          
                                            <div class="col-sm-10">
                                                <img id="blah" src={{$edit[0]->img_url}} alt="your image" style="max-width: 200px;margin-top: 20px"/>
                                            </div>
                                          </div>
                                          @endif
                              

{{-- 
                                          <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">BU</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="businessunit">
                                                            @foreach($businessUnit as $key=>$value)
                                                                <option value="{{ $value->bu_id }}">{{ $value->bu_name }}</option>
                                                            @endforeach
                                                            
                                                              </select>
                                                      </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Product</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control" name="template_product">
                                                                @foreach($templateProduct as $key=>$value)
                                                                    <option value="{{ $value->tmppd_id }}">{{ $value->tmppd_name }}</option>
                                                                @endforeach
                                                                
                                                                  </select>
                                                      </div>
                
                                               
                                              </div>

                                              <div class="form-group">
                                                <label for="inputPassword3" class="col-sm-2 control-label">Template Redeem</label>
                              
                                                <div class="col-sm-10">
                                                        <select class="form-control"  name="template_redeem">
                                                                @foreach($templateRedeem as $key=>$value)
                                                                    <option value="{{ $value->tmprd_id }}">{{ $value->tmprd_name }}</option>
                                                                @endforeach
                                                                
                                                                  </select>
                                                      </div>
                
                                               
                                              </div> --}}



            
                                          

                            
    
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <a href="{{ url('manage/product') }}"  class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                          </form>
              
            </div>
            <div class="col-sm-2"></div>

        </div>
           
                  

    </div><!-- /.box-body -->

  </div><!-- /.box -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>

    <script>
$('#blah').hide()
    function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function() {
readURL(this);
$('#blah').show()
});
            </script>
            
@stop