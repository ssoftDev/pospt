@extends('adminlte::page')

@section('title', 'Dashboard')



@section('content_header')

@if (flashMe()->ok())
  {!! flashMe_flash() !!}
@endif
    <h1>Dashboard <small>หน้าหลัก</small></h1>
    
@stop

@section('content')

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop